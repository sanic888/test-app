const Config = require('webpack-config').Config;
const environment = require('webpack-config').environment;
const helpers = require('./config/helpers');

environment.setAll({
	env: () => process.env.NODE_ENV
});

module.exports = new Config().extend(helpers.root('config', 'webpack.[env].config.js'));
