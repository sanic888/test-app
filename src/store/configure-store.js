import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import reducer from '../reducers/index';

let store;

export default function configureStore(initialState) {
	const composeEnhancers = process.env.NODE_ENV !== 'production' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

	if (!store) {
		store = createStore(
			reducer,
			initialState,
			composeEnhancers(applyMiddleware(thunk))
		);
	}

	return store;
}

export { store };
