import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route } from 'react-router-dom';
import 'es6-promise/auto';

import App from './containers/app';
import configureStore from './store/configure-store';
import history from './core/history';
import './styles/style.scss';

const initialState = window.__INITIAL_STATE__, store = configureStore(initialState);

render(
    <Provider store={store}>
        <Router history={history}>
            <Route component={App}/>
        </Router>
    </Provider>,
    document.getElementById('es-application-container')
);
