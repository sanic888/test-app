import React, { Component } from 'react';

export default class OrderForm extends Component {
	render() {
		const {text, ...rest} = this.props;

		return (<div><button type='button' {...rest}>{text}</button></div>);
	}
}
