import React, { Component } from 'react';

import './checkbox.scss';

export default class CheckBox extends Component {
	constructor(props) {
		super(props);

		this.state = { selected: !!props.selected };
	}

	onChange = (e, onClick) => {
		let selected = !this.state.selected;
		
		if(onClick) {
			selected = onClick(selected, e);
		}

		this.setState({ selected });

		e.preventDefault();
	}

	render() {
		const { text, onClick, ...rest } = this.props;

		let className = 'es-checkbox';

		if (this.state.selected) {
			className += ' selected';
		}

		return (<span className={className} onClick={(e) => this.onChange(e, onClick)} {...rest}>{text}</span>);
	}
}
