import React, { Component } from 'react';

import './radio-button.scss';

export default class RadioButton extends Component {
	constructor(props) {
		super(props);

		this.state = { selected: !!props.selected };
	}

	onChange = (e, onClick) => {
		const selected = true;

		this.setState({ selected });
		onClick && onClick(selected, e);

		e.preventDefault();
	}

	render() {
		const { text, onClick, selected, ...rest } = this.props;

		let className = 'es-radio-button';

		if (selected) {
			className += ' selected';
		}

		return (<span className={className} onClick={(e) => this.onChange(e, onClick)} {...rest}>{text}</span>);
	}
}
