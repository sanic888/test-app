import React, { Component } from 'react';

export default class Button extends Component {
	render() {
		const {text, ...rest} = this.props;

		return (<button type='button' {...rest}>{text}</button>);
	}
}
