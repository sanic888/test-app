import React, { Component } from 'react';

export default class Exchange extends Component {
	constructor(props) {
		super(props);

		this.toggleExchange = this.toggleExchange.bind(this);

		this.state = {selected: true};
	}

	componentDidMount() {
		const {selected} = this.props;

		this.setState({selected: selected});
	}

	toggleExchange(e, onClick, currency) {
		const selected = !this.state.selected;

		this.setState({selected});
		onClick && onClick(currency, selected);
		e.preventDefault();
	}

	render() {
		const {name, onClick, ...rest} = this.props;
		let className = 'es-exchange-button';

		if (this.state.selected) {
			className += ' selected';
		}

		return (
			<button type='button' className={className} onClick={(e) => this.toggleExchange(e, onClick, name)} {...rest}>{name}</button>
		);
	}
}
