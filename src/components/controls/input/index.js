import React, { Component } from 'react';

export default class Input extends Component {
	constructor(props) {
		super(props);

		this.focus = this.focus.bind(this);
		this.blur = this.blur.bind(this);
		this.click = this.click.bind(this);

		this.state = { spanClass: 'placeholder' };
	}

	componentWillMount = () => {
		const { defaultValue } = this.props;

		if (defaultValue) {
			this.setState({ spanClass: 'placeholder selected' });
		}
	};

	componentWillReceiveProps({ defaultValue }) {
		if (defaultValue) {
			this.setState({ spanClass: 'placeholder selected' });
		}
	}

	componentDidMount() {
		if (this.props.autoFocus) {
			this.input.focus();
		}
	}

	focus() {
		this.setState({ spanClass: 'placeholder selected' });
	}

	blur(e) {
		if (!e.target.value) {
			this.setState({spanClass: 'placeholder'});
		}
	}

	click() {
		this.input.focus();
	}

	render() {
		const { placeholder, ...rest } = this.props;
		const style = {
			display: rest.required ? 'block' : 'none',
			color: 'red',
			position: 'absolute',
			top: '10px',
			right: '10px'
		};

		return (
			<div className='controls-input' onClick={this.click}>
				<span className={this.state.spanClass}>{placeholder}</span>
				<input {...rest} ref={(input) => this.input = input} onBlur={this.blur} onFocus={this.focus}/>
				<span style={style}>*</span>
			</div>
		);
	}
}
