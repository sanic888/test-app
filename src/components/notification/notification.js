import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { ERROR } from '../../actions/notification';

export default class Notification extends Component {
    static propTypes = {
        notificationType: PropTypes.string.isRequired,
        message: PropTypes.string.isRequired,
        onCloseBtnClick: PropTypes.func.isRequired
    };

    componentDidMount() {
        const props = this.props;
        this.timeoutId = props.notificationType !== ERROR ? setTimeout(() => props.onCloseBtnClick(), 5000) : null;
    }

    componentWillUnmount() {
        if (this.timeoutId) {
            clearTimeout(this.timeoutId);
        }
    }

    render() {
        const props = this.props;
        return (
            <div className={`es-notification ${props.notificationType.toLowerCase()}`}>
                {props.message}
                <button
                    type="button"
                    className="es-notification-close-btn"
                    aria-label="Close"
                    onClick={props.onCloseBtnClick}>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        );
    }
}
