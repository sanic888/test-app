import React, { Component } from 'react';

import i18n from '../../core/i18n';

export default class Info extends Component {
	render() {
		return (<div><h3>{i18n.translate('Info.stub')}</h3></div>);
	}
}
