import React, { Component } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';

const mapStateToProps = state => ({ user: state.user });

@connect(mapStateToProps)
export default class ProtectedRoute extends Component {
    render() {
        const {user, component: Component} = this.props;
        return <Route render={props => user.isAuthenticated ? <Component {...props}/> : <Redirect to='/chart'/>}/>;
    }
}
