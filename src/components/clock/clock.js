import React, { Component } from 'react';

export default class Clock extends Component {
    state = { timeString: this.getTimeString() };

    componentDidMount() {
        this.intervalId = setInterval(() => {
            this.setState({ timeString: this.getTimeString() });
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.intervalId);
    }

    getTimeString() {
        const date = new Date();

        return `${this.formatTime(date.getHours())}:${this.formatTime(date.getMinutes())}:${this.formatTime(date.getSeconds())}`;
    }

    formatTime(d) {
        return d < 10 ? `0${d}` : d;
    }

    render() {
        return (<div className='es-clock-time-string'>{this.state.timeString}</div>);
    }
}
