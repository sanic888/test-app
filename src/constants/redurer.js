export const PUT_USER_TRADES = 'PUT_USER_TRADES';
export const PUT_USER_INFO = 'PUT_USER_INFO';
export const PUT_OPEN_ORDERS = 'PUT_OPEN_ORDERS';
export const PUT_MEDIANS = 'PUT_MEDIANS';
export const PUBLISH = 'PUBLISH';
export const SUBSCRIBE = 'SUBSCRIBE';
