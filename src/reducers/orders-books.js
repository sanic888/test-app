import { UPDATE_ORDERS_BOOKS } from '../actions/api';

export default (state = {}, action) => {
	switch (action.type) {
		case UPDATE_ORDERS_BOOKS:
			return Object.assign({}, state, {
				[action.payload.exchange]: action.payload.ordersBook
			});
		default:
			return state;
	}
};
