import { UPDATE_TRADES } from '../actions/api';

export default (state = {}, action) => {
	switch (action.type) {
		case UPDATE_TRADES: {
			const sell = [], buy = [];

			action.payload.trades.forEach(trade => {
				if (trade.type === 'sell') {
					sell.push(trade);
				} else {
					buy.push(trade);
				}
			});

			return Object.assign({}, state, {
				[action.payload.exchange]: { sell, buy }
			});
		}
		default:
			return state;
	}
};
