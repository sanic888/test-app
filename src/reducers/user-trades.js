import { PUT_USER_TRADES } from '../constants/redurer';

export default (state = [], action) => {
	switch (action.type) {
		case PUT_USER_TRADES:
			return action.payload;
		default:
			return state;
	}
};
