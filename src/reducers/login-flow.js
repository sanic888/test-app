import { SELECT_LOGIN_FLOW, REGISTER, LOGIN, RESET_PASSWORD } from '../actions/state';

const loginFlows = {
    REGISTER,
    LOGIN,
    RESET_PASSWORD
};

export default (state = null, action) => {
    switch (action.type) {
        case SELECT_LOGIN_FLOW:
            return loginFlows[action.flow] || null;
        default:
            return state;
    }
};
