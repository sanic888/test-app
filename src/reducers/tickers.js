import { UPDATE_TICKERS } from '../actions/api';

export default (state = {}, action) => {
	switch (action.type) {
		case UPDATE_TICKERS: {
			return Object.assign({}, state, {
				[action.payload.exchange]: action.payload.tickers
			});
		}
		default:
			return state;
	}
};
