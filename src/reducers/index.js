import { combineReducers } from 'redux';

import user from './user';
import events from './event';
import notifications from './notification';
import currencies from './currency';
import trades from './trades';
import tickers from './tickers';
import ordersBooks from './orders-books';
import chartTypes from './chart-types';
import loginFlow from './login-flow';
import exchanges from './exchanges';

export default combineReducers({
	user,
	events,
	notifications,
	currencies,
	trades,
	ordersBooks,
	chartTypes,
	loginFlow,
	tickers,
	exchanges
});
