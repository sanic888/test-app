export const EXMO_ID = 'exmo';
export const BITTREX_ID = 'bittrex';
export const EXMO_NAME = 'Exmo';
export const BITTREX_NAME = 'Bittrex';

const defaultState = [
    { id: EXMO_ID, name: EXMO_NAME },
    { id: BITTREX_ID, name: BITTREX_NAME }
];

export default (state = defaultState, action) => {
    switch (action.type) {
        default:
            return state;
    }
};
