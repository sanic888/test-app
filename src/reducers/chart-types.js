import { SELECT_CHART_TYPE } from '../actions/state';

export const LINE_CHART = 'line';
export const BRUSH_CHART = 'brush';
export const POINTS_LINE_CHART = 'pointsLine';
export const POINTS_LINE_DEV_CHART = 'pointsLineDev';

const defaultState = [
    { id: LINE_CHART, selected: true },
    { id: BRUSH_CHART, selected: false },
    { id: POINTS_LINE_CHART, selected: false },
    { id: POINTS_LINE_DEV_CHART, selected: false }
];

export default (state = defaultState, action) => {
    switch (action.type) {
        case SELECT_CHART_TYPE:
            return state.map(chart => {
                chart.selected = chart.id === action.id;
                return chart;
            });
        default:
            return state;
    }
};
