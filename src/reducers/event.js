import { PUBLISH, SUBSCRIBE } from '../constants/redurer';

export default (state = [], action) => {
	switch (action.type) {
		case PUBLISH:
			return [
				...state,
				{ name: action.name }
			];
		case SUBSCRIBE:
		default:
			return state;
	}
};
