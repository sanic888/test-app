import {
	LOGIN_SUCCESS,
	LOGOUT_SUCCESS,
	UPDATE_USER_INFO
} from '../actions/api';

const userInfo = window.initJSON;
const initialState = userInfo.isAuthenticated ? userInfo : { isAuthenticated: false };

export default function userstate(state = initialState, action) {
	switch (action.type) {
		case LOGIN_SUCCESS:
			return {
				isAuthenticated: true,
				...action.payload
			};
			
		case UPDATE_USER_INFO:
			return {
				isAuthenticated: true,
				...action.payload
			};
		case LOGOUT_SUCCESS:
			return {
				isAuthenticated: false
			};
		default:
			return state;
	}
}
