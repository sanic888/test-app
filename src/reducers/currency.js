import { UPDATE_CURRENCIES } from '../actions/api';
import { TOGGLE_CURRENCY } from '../actions/state';

export default (state = [], action) => {
	switch (action.type) {
		case UPDATE_CURRENCIES:
			return action.payload.map((currency) => {
				return {
					name: currency,
					selected: true
				};
			});
		case TOGGLE_CURRENCY:
			return state.map(currency => {
				if (currency.name === action.currencyName) {
					currency.selected = !currency.selected;
				}

				return currency;
			});
		default:
			return state;
	}
};
