import { PUSH, REMOVE } from '../actions/notification';
import utils from '../core/utils';

export default (state = [], action) => {
	switch (action.type) {
		case PUSH:
			return [
				...state,
				{
					id: utils.guidGenerator(),
					message: action.message,
                    notificationType: action.notificationType
				}
			];
		case REMOVE:
			return state.filter(item => item.id !== action.id);
		default:
			return state;
	}
};
