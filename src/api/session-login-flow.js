import fetch from 'isomorphic-fetch';

import BaseApi from './base-api';
import actionsApi from '../actions/api';
import actionsState from '../actions/state';
import actionsNotification from '../actions/notification';
import history from '../core/history';
import i18n from '../core/i18n';

class SessionLoginFlow extends BaseApi {
    constructor() {
        super();

        this.endPoints = {
            POST_SIGNIN: 'user/login',
            POST_SIGNUP: 'user/register',
            POST_REGISTER: 'user/set-password',
            POST_RESET_PASS: 'user/reset-password',
            POST_SET_RESET_PASS: 'user/set-reset-password',
            POST_LOGOUT: 'user/logout'
        };
    }

    signIn(payload) {
        return fetch(
            this.prepareEndPoint(this.endPoints.POST_SIGNIN),
            {
                method: this.requestMethods.POST,
                headers: this.headers,
                credentials: 'include',
                body: JSON.stringify(payload)
            })
            .then(res => this.parseResponse(res))
            .then(user => this.dispatch(actionsApi.loginSuccess(user)))
            .catch(error => this.onError(error));
    }

    signUp(data) {
        return fetch(
            this.prepareEndPoint(this.endPoints.POST_SIGNUP),
            {
                method: this.requestMethods.POST,
                headers: this.headers,
                body: JSON.stringify({
                    email: data.email,
                    userName: data.userName,
                    fullName: data.fullName
                })
            })
            .then(res => this.parseResponse(res))
            .then(notification => notification)
            .catch(error => this.onError(error));
    }

    register(data) {
        return fetch(
            this.prepareEndPoint(this.endPoints.POST_REGISTER),
            {
                method: this.requestMethods.POST,
                headers: this.headers,
                body: JSON.stringify({
                    password: data.password,
                    token: data.token
                })
            })
            .then(res => this.parseResponse(res))
            .then(user => {
                this.dispatch(actionsApi.loginSuccess(user));
                this.dispatch(actionsNotification.addSuccess(
                    i18n.translate('SessionLoginFlow.registerSuccess', { email: user.email })
                ));
                this.dispatch(actionsState.changeLoginFlow(null));

                history.push('/chart');
            })
            .catch(error => this.onError(error));
    }

    setResetPassword(data) {
        return fetch(
            this.prepareEndPoint(this.endPoints.POST_SET_RESET_PASS),
            {
                method: this.requestMethods.POST,
                headers: this.headers,
                body: JSON.stringify({
                    password: data.password,
                    token: data.token
                })
            })
            .then(res => this.parseResponse(res))
            .then(user => {
                this.dispatch(actionsApi.loginSuccess(user));
                history.push('/chart');
            })
            .catch(error => this.onError(error));
    }

    resetPassword(data) {
        return fetch(
            this.prepareEndPoint(this.endPoints.POST_RESET_PASS),
            {
                method: this.requestMethods.POST,
                headers: this.headers,
                body: JSON.stringify({ email: data.email })
            })
            .then(res => {
                res.json().then(
                    notification => {
                        if (res.ok) {
                            this.dispatch(actionsNotification.addSuccess(
                                notification.message || i18n.translate('SessionLoginFlow.resetPassSuccess')
                            ));
                            this.dispatch(actionsState.changeLoginFlow(null));
                        } else {
                            this.dispatch(actionsNotification.addError(
                                notification.message || i18n.translate('SessionLoginFlow.resetPassFail')
                            ));
                        }
                    }
                );
            })
            .catch(error => this.onError(error));
    }

    logout() {
        return fetch(this.prepareEndPoint(this.endPoints.POST_LOGOUT), { method: this.requestMethods.POST })
            .then(res => {
                if (res.ok) {
                    this.dispatch(actionsApi.logoutSuccess());
                } else {
                    throw {};
                }
            })
            .catch(error => this.onError(error));
    }
}

export default new SessionLoginFlow();
