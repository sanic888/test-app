import fetch from 'isomorphic-fetch';

import BaseApi from './base-api';
import actionsApi from '../actions/api';

class RestApi extends BaseApi {
    constructor() {
        super();

        this.endPoints = {
            GET_TRADES: 'init/trades',
            GET_TICKERS: 'init/tickers',
            GET_ORDERS_BOOK: 'init/order_book',
            GET_CURRENCIES: 'init/currencies',
            GET_LOCALE: 'init/locale',
            PUT_PROFILE: 'profile',
            GET_PROFILE: 'profile'
        };
    }

    getTrades() {
        return fetch(this.prepareEndPoint(this.endPoints.GET_TRADES), { method: this.requestMethods.GET })
            .then(res => this.parseResponse(res))
            .then(
                trades => {
                    this.dispatch(actionsApi.updateTrades(this.exchanges.EXMO, trades.exmo));
                    this.dispatch(actionsApi.updateTrades(this.exchanges.BITTREX, trades.bittrex));
                }
            )
            .catch(error => this.onError(error));
    }

    getTickers() {
        return fetch(this.prepareEndPoint(this.endPoints.GET_TICKERS), { method: this.requestMethods.GET })
            .then(res => this.parseResponse(res))
            .then(
                tickers => {
                    this.dispatch(actionsApi.updateTickers(this.exchanges.EXMO, tickers.exmo));
                    this.dispatch(actionsApi.updateTickers(this.exchanges.BITTREX, tickers.bittrex));
                    
                }
            )
            .catch(error => this.onError(error));
    }

    getOrdersBooks() {
        return fetch(
            this.prepareEndPoint(this.endPoints.GET_ORDERS_BOOK),
            {
                method: this.requestMethods.GET,
                headers: this.headers
            })
            .then(res => this.parseResponse(res))
            .then(
                ordersBooks => {
                    this.dispatch(actionsApi.updateOrdersBook(this.exchanges.EXMO, ordersBooks.exmo));
                    this.dispatch(actionsApi.updateOrdersBook(this.exchanges.BITTREX, ordersBooks.bittrex));
                }
            )
            .catch(error => this.onError(error));
    }

    getCurrencies() {
        return fetch(
            this.prepareEndPoint(this.endPoints.GET_CURRENCIES),
            {
                method: this.requestMethods.GET,
                headers: this.headers
            })
            .then(res => this.parseResponse(res))
            .then(currencies => this.dispatch(actionsApi.updateCurrencies(currencies)))
            .catch(error => this.onError(error));
    }

    getLocale(locale) {
        return fetch(`${this.prepareEndPoint(this.endPoints.GET_LOCALE)}?lang=${locale}`)
            .then(res => this.parseResponse(res))
            .catch(error => this.onError(error));
    }

    getProfile() {
        return fetch(
            this.prepareEndPoint(this.endPoints.GET_PROFILE),
            {
                method: this.requestMethods.GET,
                headers: this.headers
            })
            .then(res => this.parseResponse(res))
            .catch(error => this.onError(error));
    }

    updateProfile(data) {
        return fetch(
            this.prepareEndPoint(this.endPoints.PUT_PROFILE),
            {
                method: this.requestMethods.PUT,
                headers: this.headers,
                body: JSON.stringify({
                    userName: data.userName,
                    fullName: data.fullName
                })
            })
            .catch(error => this.onError(error));
    }

}

export default new RestApi();
