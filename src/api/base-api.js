import config from '../../config/app.config';
import { store } from '../store/configure-store';
import actionsNotification from '../actions/notification';

export default class BaseApi {
    constructor() {
        this.baseUrl = config.web.basePath;
        this.webSocketUrl = `${config.socket.host}:${config.socket.port}`;
        this.requestMethods = {
            POST: 'POST',
            PUT: 'PUT',
            GET: 'GET'
        };
        this.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };
        this.exchanges = {
            EXMO: 'exmo',
            BITTREX: 'bittrex'
        };
        this.endPoints = {};
    }

    onError(error) {
        this.dispatch(actionsNotification.addError(error.message || 'Server error'));
        throw error;
    }

    prepareEndPoint(url) {
        return `${this.baseUrl}${url}`;
    }

    parseResponse(response) {
        if (response.ok) {
            return response.json().then(res => res);
        } else {
            throw { message: response.statusText || 'Server error' };
        }
    }

    dispatch(action) {
        return store.dispatch(action);
    }
}
