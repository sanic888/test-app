import openSocket from 'socket.io-client';

import BaseApi from './base-api';
import actionsApi from '../actions/api';

class WebSocketApi extends BaseApi {
	constructor() {
		super();

		this.endPoints = {
			UPDATE_EXMO_ORDER_BOOK: 'socket:exmo:order-book:update',
			UPDATE_EXMO_TRADES: 'socket:exmo:trades:update',
			UPDATE_EXMO_TICKERS: 'socket:exmo:ticker:update',
			UPDATE_BITTRES_ORDER_BOOK: 'socket:bittrex:order-book:update',
			UPDATE_BITTREX_TRADES: 'socket:bittrex:trades:update',
			UPDATE_BITTREX_TICKERS: 'socket:bittrex:ticker:update'
		};
		this.socket = this.openConnection();
	}

	openConnection() {
		return openSocket(this.webSocketUrl);
	}

	subscribe() {
		/**
		 * Exmo exchange
		 * */
		this.socket.on(
			this.endPoints.UPDATE_EXMO_ORDER_BOOK,
			ordersBook => this.dispatch(actionsApi.updateOrdersBook(this.exchanges.EXMO, ordersBook))
		);
		this.socket.on(
			this.endPoints.UPDATE_EXMO_TRADES,
			trades => this.dispatch(actionsApi.updateTrades(this.exchanges.EXMO, trades))
		);
		this.socket.on(
			this.endPoints.UPDATE_EXMO_TICKERS,
			tickers => this.dispatch(actionsApi.updateTickers(this.exchanges.EXMO, tickers))
		);

		/**
		 * Bittrex exchange
		 * */
		this.socket.on(
			this.endPoints.UPDATE_BITTRES_ORDER_BOOK,
			ordersBook => this.dispatch(actionsApi.updateOrdersBook(this.exchanges.BITTREX, ordersBook))
		);
		this.socket.on(
			this.endPoints.UPDATE_BITTREX_TRADES,
			trades => this.dispatch(actionsApi.updateTrades(this.exchanges.BITTREX, trades))
		);
		this.socket.on(
			this.endPoints.UPDATE_BITTREX_TICKERS,
			tickers => this.dispatch(actionsApi.updateTickers(this.exchanges.BITTREX, tickers))
		);
	}
}

export default new WebSocketApi();
