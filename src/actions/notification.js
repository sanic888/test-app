export const PUSH = 'PUSH';
export const REMOVE = 'REMOVE';

export const ERROR = 'ERROR';
const SUCCESS = 'SUCCESS';

class Notification {
	addSuccess = message => {
		return {
			type: PUSH,
            notificationType: SUCCESS,
			message
		};
	};

	addError = message => {
		return {
			type: PUSH,
            notificationType: ERROR,
			message
		};
	};

	remove = id => {
		return {
			type: REMOVE,
			id
		};
	};
}

export default new Notification();
