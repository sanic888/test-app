export const TOGGLE_CURRENCY = 'TOGGLE_CURRENCY';
export const SELECT_CHART_TYPE = 'SELECT_CHART_TYPE';
export const SELECT_LOGIN_FLOW = 'SELECT_LOGIN_FLOW';

export const REGISTER = 'REGISTER';
export const LOGIN = 'LOGIN';
export const RESET_PASSWORD = 'RESET_PASSWORD';

class State {
    toggleCurrency = currencyName => {
        return {
            type: TOGGLE_CURRENCY,
            currencyName
        };
    };

    changeSelectedChartType = id => {
        return {
            type: SELECT_CHART_TYPE,
            id
        };
    };

    changeLoginFlow = flow => {
        return {
            type: SELECT_LOGIN_FLOW,
            flow
        };
    };
}

export default new State();
