export const UPDATE_TRADES = 'UPDATE_TRADES';
export const UPDATE_TICKERS = 'UPDATE_TICKERS';
export const UPDATE_ORDERS_BOOKS = 'UPDATE_ORDERS_BOOKS';
export const UPDATE_CURRENCIES = 'UPDATE_CURRENCIES';

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const UPDATE_USER_INFO = 'UPDATE_USER_INFO';

class Api {
    updateTrades(exchange, trades) {
        return {
            type: UPDATE_TRADES,
            payload: {
                exchange,
                trades
            }
        };
    }
    
    updateTickers(exchange, tickers) {
        return {
            type: UPDATE_TICKERS,
            payload: {
                exchange,
                tickers
            }
        };
    }

    updateOrdersBook(exchange, ordersBook) {
        return {
            type: UPDATE_ORDERS_BOOKS,
            payload: {
                exchange,
                ordersBook
            }
        };
    }

    updateCurrencies(currencies) {
        return {
            type: UPDATE_CURRENCIES,
            payload: currencies.map(currency => currency.name)
        };
    }

    loginSuccess(json) {
        return {
            type: LOGIN_SUCCESS,
            payload: json
        };
    }

    logoutSuccess() {
        return {
            type: LOGOUT_SUCCESS
        };
    }    

    updateUserInfo(data) {
        return {
            type: UPDATE_USER_INFO,
            payload: data
        };
    }
}

export default new Api();
