import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ApiActions from '../../actions/api';
import i18n from '../../core/i18n';

export class CurrencyPage extends Component {
	constructor(props) {
		super(props);

		this.getTable = this.getTable.bind(this);
		this.setCurrency = this.setCurrency.bind(this);
		this.setPortion = this.setPortion.bind(this);
		this.getOrdersBooksVolumeChart = this.getOrdersBooksVolumeChart.bind(this);

		this.state = {
			currenciesPairs: ['KICK_BTC', 'ETH_LTC'],
			portions: [1, 0.5, 0.2, 0.1],
			selectedPortion: 0.5,
			currency: 'ETH_LTC',
			exchanges: [
				{ id: 1, name: 'Exmo' },
				{ id: 2, name: 'Bittrex' }
			],
			selectedExchange: 'exmo'
		};
	}

	static propTypes = {
		trades: PropTypes.object,
		ordersBooks: PropTypes.object,
		currencies: PropTypes.array
	};

	componentDidMount() {
		/* this.props.apiActions.getCurrencies();
		this.getTradesSetIntervalId = setInterval(() => this.props.apiActions.getTrades(this.state.currency), 1 * 1000);
		this.getOrderBookSetIntervalId = setInterval(() => this.props.apiActions._getOrderBook(this.state.currency), 1 * 1000);*/
	}

	componentWillUnmount() {
		/* this.getTradesSetIntervalId && clearInterval(this.getTradesSetIntervalId);
		this.getOrderBookSetIntervalId && clearInterval(this.getOrderBookSetIntervalId);*/
	}

	getTable(trades) {
		const classNames = ['first-line', 'second-line'];
		let lastMin = 0, lastClass = 0;
		const trs = trades.map(trade => {
			const date = (new Date(trade.date * 1000)),
				hours = date.getHours() > 9 ? date.getHours() : "0" + date.getHours().toString(),
				minutes = date.getMinutes() > 9 ? date.getMinutes() : "0" + date.getMinutes().toString(),
				seconds = date.getSeconds() > 9 ? date.getSeconds() : "0" + date.getSeconds().toString(),
				dateString = `${hours}:${minutes}:${seconds}`;

			if (lastMin !== minutes) {
				lastClass = +!lastClass;
			}

			lastMin = minutes;

			const className = classNames[lastClass];

			return (<tr key={trade.trade_id} className={className}>
				<td>{trade.price}</td>
				<td>{trade.quantity}</td>
				<td>{trade.amount}</td>
				<td>{trade.type}</td>
				<td>{dateString}</td>
			</tr>);
		});

		return (<table className='trades-table'>
			<thead>
			<tr>
				<th>{i18n.translate('Currency.thLabelPrice')}</th>
				<th>{i18n.translate('Currency.thLabelQuantity')}</th>
				<th>{i18n.translate('Currency.thLabelAmount')}</th>
				<th>{i18n.translate('Currency.thLabelType')}</th>
				<th>{i18n.translate('Currency.thLabelDate')}</th>
			</tr>
			</thead>
			<tbody>{trs}</tbody>
		</table>);
	}

	setPortion(e) {
		this.setState({ selectedPortion: e.target.textContent });
	}

	getOrdersBooksVolumeChart(ordersBooks) {
		if (!ordersBooks) return;

		const price = ordersBooks.sell_top,
			result = {},
			getPer = (item) => {
				const per = Math.ceil(item.price * (100 / this.state.selectedPortion) / price - 100 / this.state.selectedPortion);
				// return (per * this.state.selectedPortion).toString();
				return (Math.round(10 * per * this.state.selectedPortion) / 10).toString();
			};

		ordersBooks.sell.forEach(item => {
			const per = getPer(item);// Math.ceil(item[0] * 100 / price - 100).toString();

			!result[per] && (result[per] = {
				value: item.price,
				itemsFirst: [],
				itemsSecond: []
			});

			result[per].itemsFirst.push(+item.quantity);
			result[per].itemsSecond.push(+item.amount);
		});

		const allKeys = Object.keys(result)
			.sort((a, b) => {
				if (parseFloat(a) > parseFloat(b)) return 1;
				if (parseFloat(a) < parseFloat(b)) return -1;
			})
			.map(item => item.toString());

		allKeys.forEach(per => {
			result[per].sumFirst = result[per].itemsFirst.reduce((p, c) => p + c, 0);
			result[per].sumSecond = result[per].itemsSecond.reduce((p, c) => p + c, 0);
		});

		const volumes = allKeys.map(per => result[per].sumFirst),
			maxSum = Math.max.apply(Math, volumes),
			maxWidth = 150,
			volumeChartPieces = allKeys.map((per) => {
				const item = result[per];

				return (<div className='volume-chart-piece-wrapper' key={per}>
					<div className='volume-chart-piece' style={{ width: maxWidth * item.sumFirst / maxSum }}>
						<div className='volume-chart-piece-per'>{per}%</div>
					</div>
					<div>{item.sumFirst} / {item.sumSecond}</div>
					<div> | {per}% / {item.value}</div>
				</div>);
			});

		return (<div className='volume-chart'>
			<div className='volume-chart-portions'>
				<div>portions:</div>
				{this.state.portions.map(portion =>
					<div className='volume-chart-portions-piece' key={portion} onClick={this.setPortion}>{portion}</div>
				)}
			</div>
			{volumeChartPieces}
		</div>);
	}

	setCurrency(e) {
		this.setState({ currency: e.target.textContent });
	}

	changeExchange(name) {
		this.setState({ selectedExchange: name.toLowerCase() });
	}

	render() {
		const tradesBuy = this.getTable(this.props.trades.buy.filter(item => item.pair === this.state.currency));
		const tradesSell = this.getTable(this.props.trades.sell.filter(item => item.pair === this.state.currency));

		const ordersBooks = this.props.ordersBooks[this.state.selectedExchange].filter((item) => item.pair === this.state.currency)[0];
		const ordersBooksVolumeChart = this.getOrdersBooksVolumeChart(ordersBooks);

		const exchanges = this.state.exchanges.map((exchange) => {
			return (<label key={exchange.name} className={exchange.name.toLowerCase() === this.state.selectedExchange.toLowerCase() ? 'selected radio-btn-container' : 'radio-btn-container'}>
				<input type="radio"
				       value={exchange.name}
				       checked={exchange.name.toLowerCase() === this.state.selectedExchange.toLowerCase()}
				       onChange={() => this.changeExchange(exchange.name)}/>
				<span className='name'>{exchange.name}</span>
				<span className='radio-btn'/>
			</label>);
		});

		return (
			<div>
				<div className='exchanges'>{exchanges}</div>
				<div className='currencies-menu'>
					{this.props.currencies.map((currency, i) =>
						<div key={i}
						     className={currency.name === this.state.currency ? 'currencies-menu-item selected' : 'currencies-menu-item'}
						     onClick={this.setCurrency}>
							{currency.name}
						</div>
					)}
				</div>
				<div className='trades-wrapper'>
					<div className='scroll-container'>{tradesBuy}</div>
					<div className='scroll-container'>{tradesSell}</div>
					{ordersBooksVolumeChart}
				</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		currencies: state.currencies,
		trades: state.trades.exmo,
		ordersBooks: state.ordersBooks
	};
}

function mapDispatchToProps(dispatch) {
	return { apiActions: bindActionCreators(ApiActions, dispatch) };
}

export default connect(mapStateToProps, mapDispatchToProps)(CurrencyPage);
