import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import * as d3 from 'd3';

import i18n from '../../core/i18n';

export class LineChartSvgPage extends Component {
	constructor(props) {
		super(props);

		this.createLineChart = this.createLineChart.bind(this);
	}

	static propTypes = {
		trades: PropTypes.object,
		currency: PropTypes.string
	};

	componentDidMount() {
		this.prevLength = this.props.trades.sell.length;

		this.createLineChart(this.props.trades.sell, this.props.trades.buy);
	}

	_cleanSvg() {
		this.node.childNodes.forEach(node => this.node.removeChild(node));
	}

	_cleanSvgV1(className) {
		// this.node.childNodes.forEach(node => this.node.removeChild(node));
		const elems = document.getElementsByClassName(className);
		for (let i = 0; i < elems.length; i++) {
			elems[i].parentNode.removeChild(elems[i]);
		}
	}

	groupBy(source, key1, key2) {
		const res = {};

		source.forEach(s => {
			res[`${s[key1]}${s[key2]}`] = res[`${s[key1]}${s[key2]}`] || [];
			res[`${s[key1]}${s[key2]}`].push(s);
		});

		return res;
	}

	createLineChart(trades) {
		if (!this.node || !trades.length || !this.props.currency) return;

		const className = this.props.currency;

		trades = trades.slice(0, this.prevLength);

		const toString1 = date => {
			const prettyTime = n => n < 10 ? `0${n}` : n;

			return `${prettyTime(date.getHours())}:${prettyTime(date.getMinutes())}:${prettyTime(date.getSeconds())}`;
		};

		const margin = {top: 30, right: 30, bottom: 40, left: 50},
			width = 960 - margin.left - margin.right,
			height = 500 - margin.top - margin.bottom;

		const x = d3.scaleTime().range([0, width]), y = d3.scaleLog().range([height, 0]);

		// title for X line
		const xAxis = d3.axisBottom()
			.scale(x)
			.tickSize(-height, 0)
			.tickFormat(x => toString1(x));

		let toFixedNumber = 2;
		// title for Y line
		const yAxis = d3.axisLeft()
			.scale(y)
			.tickSize(-width, 0)
			.tickFormat(y => {
				const val = y.toString().split('.')[1] || "";
				const l = val.length;

				if (l > toFixedNumber) {
					toFixedNumber = l;
				}

				return y;
			});

		const line = d3.line()
			.x(d => x(d.date))
			.y(d => y(d.close));

		this._cleanSvg(className);

		const tooltip = d3.select(this.tooltip), infoTab = d3.select(this.infoTab);

		const svg = d3.select(this.node)
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.append("g")
			.attr('class', className)
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		const gX = svg.append("g")
			.attr("class", `axis axis--x ${className}`)
			.attr("transform", "translate(0," + height + ")");
		const gY = svg.append("g").attr("class", `axis axis--y ${className}`);

		let data;
		const render = (_data) => {
			const drowLine = (data, className = '') => {
				svg.append("path")
					.datum(data)
					.attr("class", `line ${className}`)
					.attr("d", line);
			};
			data = _data.filter(t => t.pair === this.props.currency).map(t => {
				return {
					date: new Date(t.date * 1000),
					close: +t.price,
					amount: t.amount,
					quantity: t.quantity
				};
			}).sort((a, b) => {
				if (a.date > b.date) {
					return 1;
				} else {
					return -1;
				}
			});
			/* const dataBuy = tradesBuy.filter((t) => t.pair === this.props.currency).map(t => {
				return {
					date: new Date(t.date * 1000),
					close: +t.price,
					amount: t.amount,
					quantity: t.quantity
				};
			}).sort((a, b) => {
				if (a.date > b.date) {
					return 1;
				} else {
					return -1;
				}
			});*/
			x.domain(d3.extent(data, d => d.date));
			const minY = d3.min(data, function(d) {
				return d.close;
			});
			const maxY = d3.max(data, function(d) {
				return d.close;
			});
			const diffY = (maxY - minY) * 0.1;
			y.domain([minY - diffY, maxY + diffY]);

			yAxis.tickValues(d3.scaleLinear()
				.domain(y.domain())
				.ticks(10));

			gX.call(xAxis);

			gY.call(yAxis)
				.selectAll('.tick')
				.classed('tick--one', d => Math.abs(d - 1) < 1e-6);

			drowLine(data, 'sell');
			// drowLine(dataBuy, 'buy');
		};

		render(trades);

		const mouseG = svg.append("g").attr("class", `mouse-over-effects ${className}`);

		mouseG.append("path") // this is the black vertical line to follow mouse
			.attr("class", `mouse-line ${className}`)
			.style("stroke", "#1f77b4")
			.style("stroke-width", "1px")
			.style("opacity", "0");

		mouseG.append("path") // this is the black vertical line to follow mouse
			.attr("class", `mouse-line-gor ${className}`)
			.style("stroke", "#1f77b4")
			.style("stroke-width", "1px")
			.style("opacity", "0");

		const lines = this.node.getElementsByClassName('line');

		let mousePerLine = mouseG.selectAll('.mouse-per-line')
			.data([{name: 'test', values: data}])
			.enter()
			.append("g")
			.attr("class", `mouse-per-line ${className}`);

		mousePerLine.append("circle")
			.attr("r", 7)
			.attr('class', className)
			.style("stroke", () => "black")
			.style("fill", "none")
			.style("stroke-width", "1px")
			.style("opacity", "0");

		tooltip.style("opacity", "0");

		const mousemove = () => { // mouse moving over canvas
			const mouse = d3.mouse(window.event.target);
			svg.select(".mouse-line")
				.attr("d", () => {
					let d = "M" + mouse[0] + "," + height;
					d += " " + mouse[0] + "," + 0;

					return d;
				});

			svg.selectAll(".mouse-per-line")
				.attr("transform", (d, i) => {
					/* var xDate = x.invert(mouse[0]),
					bisect = d3.bisector(function (d) { return d.date; }).left;*/

					let beginning = 0,
						end = lines[i].getTotalLength(),
						target = null,
						pos = null;

					while (true) { // eslint-disable-line no-constant-condition
						target = Math.floor((beginning + end) / 2);
						pos = lines[i].getPointAtLength(target);

						if ((target === end || target === beginning) && pos.x !== mouse[0]) {
							break;
						}

						if (pos.x > mouse[0]) {
							end = target;
						} else if (pos.x < mouse[0]) {
							beginning = target;
						} else break; // position found
					}

					tooltip.select('.chart-tooltip-value').text(y.invert(pos.y).toFixed(toFixedNumber));
					tooltip.select('.chart-tooltip-time').text(this.getTimeString(x.invert(pos.x)));

					this.showInfo(infoTab, this.getTimeString(x.invert(pos.x)), y.invert(pos.y).toFixed(toFixedNumber));

					svg.select(".mouse-line-gor")
						.attr("d", () => {
							let d = "M0," + pos.y;
							d += " " + width + "," + pos.y;

							return d;
						});

					tooltip.style('margin-left', `${mouse[0] + 60}px`);
					tooltip.style('margin-top', `${pos.y + 30}px`);

					return "translate(" + mouse[0] + "," + pos.y + ")";
				});
		};

		mouseG.append('svg:rect') // append a rect to catch mouse movements on canvas
			.attr('class', className)
			.attr('width', width) // can't catch mouse events on a g element
			.attr('height', height)
			.attr('fill', 'none')
			.attr('pointer-events', 'all')
			.on('mousewheel', () => {
				let length;

				if (window.event.wheelDelta > 0) {
					length = this.prevLength - Math.round(this.prevLength * 0.05);

					if (length < 20) {
						length = 20;
					}
				} else {
					length = this.prevLength + Math.round(this.prevLength * 0.05);

					if (length > this.props.trades.sell.length) {
						length = this.props.trades.sell.length;
					}
				}

				if (length !== this.props.trades.sell.length) {
					this.prevLength = length;
					svg.select('.line').remove();
					render(this.props.trades.sell.slice(0, length));
					mousemove();
					window.event.preventDefault();
				}
			})
			.on('mouseout', () => {
				svg.select(".mouse-line").style("opacity", "0");
				svg.select(".mouse-line-gor").style("opacity", "0");
				svg.selectAll(".mouse-per-line circle").style("opacity", "0");

				tooltip.style("opacity", "0");
			})
			.on('mouseover', () => {
				svg.select(".mouse-line").style("opacity", "1");
				svg.select(".mouse-line-gor").style("opacity", "1");
				svg.selectAll(".mouse-per-line circle").style("opacity", "1");

				tooltip.style("opacity", "1");
			})
			.on('mousemove', mousemove);
	}

	showInfo(infoTab, date, price) { // amount, quantity
		infoTab.select('.date').text(date);
		infoTab.select('.price').text(price);
	}

	getTimeString(date) {
		const formatTime = (d) => {
			return d < 10 ? `0${d}` : d;
		};

		return `${formatTime(date.getHours())}:${formatTime(date.getMinutes())}:${formatTime(date.getSeconds())}`;
	}

	render() {
		this.createLineChart(this.props.trades.sell, this.props.trades.buy);

		const date = new Date();
		const formatTime = (d) => {
			return d < 10 ? `0${d}` : d;
		};
		const timeString = `${formatTime(date.getMonth() + 1)}/${formatTime(date.getDate())} - ${formatTime(date.getHours())}:${formatTime(date.getMinutes())}:${formatTime(date.getSeconds())}`;

		return (<div className='chart-wrapper line-chart'>
			<div className='chart-header'>
				<span className='chart-currency'>{this.props.currency}</span>
				<span className='chart-time'>{timeString}</span>
			</div>
			<svg ref={node => this.node = node} width={500} height={500}/>
			<div ref={node => this.tooltip = node} className='tooltip'>
				<span className='chart-tooltip-value'/><br/>
				<span className='chart-tooltip-time'/>
			</div>
			<div ref={node => this.infoTab = node} className='chart-info'>
				<span className='chart-info-header'>{i18n.translate('LineChartSvg.info')}</span>
				<div className='chart-info-body'>
					<span className='chart-info-body-name'>{i18n.translate('LineChartSvg.date')}</span>:
					<span className='chart-info-body-value date'/>
				</div>
				<div className='chart-info-body'>
					<span className='chart-info-body-name'>{i18n.translate('LineChartSvg.price')}</span>:
					<span className='chart-info-body-value price'/>
				</div>
				<div className='chart-info-body'>
					<span className='chart-info-body-name'>{i18n.translate('LineChartSvg.amount')}</span>:
					<span className='chart-info-body-value amount'/>
				</div>
				<div className='chart-info-body'>
					<span className='chart-info-body-name'>{i18n.translate('LineChartSvg.quantity')}</span>:
					<span className='chart-info-body-value quantity'/>
				</div>
			</div>
		</div>);
	}
}

const mapStateToProps = (state) => {
	return {trades: state.trades.exmo};
};

const mapDispatchToProps = () => {
	return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(LineChartSvgPage);
