import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import * as d3 from 'd3';
import i18n from '../../core/i18n';

export class LineChartSvgPage extends Component {
	constructor(props) {
		super(props);

		this.createLineChart = this.createLineChart.bind(this);

		this.state = {
			exchanges: {
				exmo: {
					name: 'exmo',
					color: '#c71111'
				},
				bittrex: {
					name: 'bittrex',
					color: '#384a5f'
				}
			}
		};
	}

	static propTypes = {
		currency: PropTypes.string,
		trades: PropTypes.object
	};

	componentDidMount() {
		this.createLineChart();
	}

	_cleanSvg() {
		this.node.childNodes.forEach(node => this.node.removeChild(node));
	}

	createLineChart() {
		const months = this._getDates();
		const data = this._getFormatedData(months);

		if (!this.node || !data || !data.length || !this.props.currency) return;

		this._cleanSvg();

		const svg = d3.select(this.node),
			margin = {top: 20, right: 30, bottom: 30, left: 40},
			width = svg.attr("width") - margin.left - margin.right,
			height = svg.attr("height") - margin.top - margin.bottom,
			g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		const x = d3.scaleTime().range([0, width]);
		const y = d3.scaleLinear().range([height, 0]);

		const voronoi = d3.voronoi()
			.x(d => x(d.date))
			.y(d => y(d.value))
			.extent([[-margin.left, -margin.top], [width + margin.right, height + margin.bottom]]);

		const line = d3.line()
			.x(d => x(d.date))
			.y(d => y(d.value));

		const yMin = d3.min(data, c => d3.min(c.values, d => d.value));
		const yMax = d3.max(data, c => d3.max(c.values, d => d.value));
		const diffY = (yMax - yMin) * 0.1;

		x.domain(d3.extent(months));
		y.domain([
			yMin - diffY,
			yMax + diffY
		]).nice();

		const toString1 = date => {
			const prettyTime = n => n < 10 ? `0${n}` : n;

			return `${prettyTime(date.getHours())}:${prettyTime(date.getMinutes())}:${prettyTime(date.getSeconds())}`;
		};
		// title for X line
		const xAxis = d3.axisBottom()
			.scale(x)
			.tickSize(-height, 0)
			.tickFormat(x => toString1(new Date(x)))
			.tickValues(
				d3.scaleLinear()
					.domain(x.domain())
					.ticks(7)
			);

		g.append("g")
			.attr("class", "axis axis--x")
			.attr("transform", "translate(0," + height + ")")
			.call(xAxis);

		const yAxis = d3.axisLeft()
			.scale(y)
			.tickSize(-width, 0)
			.tickFormat(y => {
				/* const val = y.toString().split('.')[1] || "";
				const l = val.length;
				if(l > toFixedNumber) {
					toFixedNumber = l;
				}*/
				return y;
			})
			.tickValues(
				d3.scaleLinear()
					.domain(y.domain())
					.ticks(10)
			);

		g.append("g")
			.attr("class", "axis axis--y")
			// .call(d3.axisLeft(y).ticks(3, "%"))
			.call(yAxis)
			.append("text")
			.attr("x", 4)
			.attr("y", -10)
			.attr("dy", "0.32em")
			.style("text-anchor", "start")
			.style("font-style", "italic")
			.style("fill", "#000")
			// .style("font-weight", "bold")
			.text(i18n.translate('PointLineChartDev.yAxisLabel'));

		// line
		const gLine = g.append("g").attr("class", 'cities sell');

		gLine.selectAll("path")
			.data(data)
			.enter().append("path")
			.attr("d", function(d) {
				d.line = this;
				return line(d.values);
			})
			// set color for each line
			.attr("stroke", d => this.state.exchanges[d.name].color);

		/* gLine.append('text')
			.attr('x', width - 8)
			.attr('y', function (d, i) {
				return (i * 20) + 9;
			})
			.text(function (d) {
				return 'TEST 1';
			});*/

		const focus = g.append("g")
			.attr("transform", "translate(-100,-100)")
			.attr("class", "focus");

		focus.append("circle").attr("r", 3.5);
		focus.append("text").attr("y", -10);

		const voronoiGroup = g.append("g").attr("class", "voronoi");

		voronoiGroup.selectAll("path")
			.data(voronoi.polygons(d3.merge(data.map(d => d.values))))
			.enter().append("path")
			.attr("d", d => d ? "M" + d.join("L") + "Z" : null)
			.on("mouseover", d => {
				d3.select(d.data.city.line).classed("city--hover", true);
				d.data.city.line.parentNode.appendChild(d.data.city.line);
				focus.attr("transform", "translate(" + x(d.data.date) + "," + y(d.data.value) + ")");
				focus.select("text").text(d.data.value);
			})
			.on("mouseout", d => {
				d3.select(d.data.city.line).classed("city--hover", false);
				focus.attr("transform", "translate(-100,-100)");
			});

		d3.select("#show-voronoi")
			.property("disabled", false)
			.on("change", () => voronoiGroup.classed("voronoi--show", this.checked));
	}

	_getFormatedData(dates) {
		const result = [];
		const exchanges = Object.keys(this.props.trades);

		exchanges.forEach((exchange) => {
			const trades = this.props.trades[exchange].sell
				.filter(t => t.pair === this.props.currency)
				.sort((a, b) => {
					if (a.date > b.date) {
						return 1;
					} else {
						return -1;
					}
				});

			const elem = {name: exchange, values: null};

			dates; // TODO: What is it?
			elem.values = trades.map(k => {
				return {
					city: elem,
					date: new Date(parseInt(k.date) * 1000),
					value: +k.price
				};
			});

			result.push(elem);
		});

		return result;
	}

	_getDates() {
		// const result = [];
		const exchanges = Object.keys(this.props.trades);
		let trades = [];

		exchanges.forEach((exchange) => {
			trades = this.props.trades[exchange].sell.filter(t => t.pair === this.props.currency).concat(trades);
		});

		const data = trades.sort((a, b) => {
			if (a.date > b.date) {
				return 1;
			} else {
				return -1;
			}
		}).map(t => parseInt(t.date)).reduce((p, c) => {
			if (!p.includes(c)) {
				p.push(c);
			}

			return p;
		}, []).map(date => new Date(date * 1000));

		return data;
	}

	render() {
		this.createLineChart();

		const exchanges = Object.keys(this.state.exchanges);
		const exchangesDesc = exchanges.map(exchange => {
			return (<div key={exchange} className='chart-info-line'>
				<div className='chart-info-line-color' style={{backgroundColor: this.state.exchanges[exchange].color}}/>
				<div className='chart-info-line-text'>&nbsp;-&nbsp;{this.state.exchanges[exchange].name}</div>
			</div>);
		});

		return (<div className='chart-wrapper points-line'>
			<svg ref={node => this.node = node} width={900} height={500}/>
			<div className='chart-info'>{exchangesDesc}</div>
		</div>);
	}
}

const mapStateToProps = (state) => {
	return {trades: state.trades};
};

const mapDispatchToProps = () => {
	return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(LineChartSvgPage);
