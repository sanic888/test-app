import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import LineChartSvg from './line-chart-svg';
import PointLineChart from './point-line-chart';
import PointLineChartDev from './point-line-chart-dev';
import BrushZoom from './brush-zoom';
import ExchangeButton from '../../components/controls/button/exchange';
import stateActions from '../../actions/state';
import { LINE_CHART, BRUSH_CHART, POINTS_LINE_CHART, POINTS_LINE_DEV_CHART } from '../../reducers/chart-types';
import i18n from '../../core/i18n';

export class ChartPage extends Component {
    constructor(props) {
        super(props);

        this.toggleCurrency = this.toggleCurrency.bind(this);
        this.changeChartType = this.changeChartType.bind(this);

        this.state = {
            exchanges: [
                { id: 1, name: 'Exmo', selected: false },
                { id: 2, name: 'Bittrex', selected: false },
                { id: 3, name: 'Binance', selected: false }
            ],
            chartTypesLabels: {
                [LINE_CHART]: i18n.translate('Chart.lineChartLabel'),
                [BRUSH_CHART]: i18n.translate('Chart.brushChartLabel'),
                [POINTS_LINE_CHART]: i18n.translate('Chart.pointsLineChartLabel'),
                [POINTS_LINE_DEV_CHART]: i18n.translate('Chart.pointsLineDevChartLabel')
            }
        };
    }

    static propTypes = {
        currencies: PropTypes.array,
        stateActions: PropTypes.object,
        chartTypes: PropTypes.array
    };

    componentDidMount() {
        this.setState({
            selectedCurrencies: [this.props.currencies[0]]
        });
    }

    toggleCurrency(currency, state) {
        console.log(currency, state);
        this.props.stateActions.toggleCurrency(currency);
    }

    changeChartType(val) {
        console.log(this.props.chartTypes);
        this.props.stateActions.changeSelectedChartType(val);
    }

    render() {
        const exchanges = this.state.exchanges.map(exchange => {
            return <ExchangeButton key={exchange.id}
                                   id={exchange.id}
                                   name={exchange.name}
                                   selected={exchange.selected}/>;
        });

        const currencies = this.props.currencies.map(currency => {
            return <ExchangeButton key={currency.name} name={currency.name} selected={currency.selected}
                                   onClick={this.toggleCurrency}/>;
        });

        const selectedChart = this.props.chartTypes.filter(chartType => chartType.selected)[0].id;

        const lineChartsSvg = this.props.currencies
            .filter(currency => currency.selected)
            .map(currency => {
                let resultChart;

                if (selectedChart === LINE_CHART) {
                    resultChart = <LineChartSvg key={currency.name} currency={currency.name}/>;
                } else if (selectedChart === BRUSH_CHART) {
                    resultChart = <BrushZoom key={currency.name} currency={currency.name}/>;
                } else if (selectedChart === POINTS_LINE_CHART) {
                    resultChart = <PointLineChart key={currency.name} currency={currency.name}/>;
                } else if (selectedChart === POINTS_LINE_DEV_CHART) {
                    resultChart = <PointLineChartDev key={currency.name} currency={currency.name}/>;
                }

                return resultChart;
            });

        const chartTypes = this.props.chartTypes
            .map(chartType => {
                return <label key={chartType.id}
                              className={chartType.selected ? 'selected radio-btn-container' : 'radio-btn-container'}>
                    <input type="radio"
                           value={chartType.id}
                           checked={chartType.selected}
                           onChange={() => this.changeChartType(chartType.id)}/>
                    <span className='name'>{this.state.chartTypesLabels[chartType.id]}</span>
                    <span className='radio-btn'/>
                </label>;
            });

        return (<div className='chart-container'>
            <div className='currencies'>{currencies}</div>
            <div className='exchanges'>{exchanges}</div>
            <div className='chart-block'>
                <div className='chart-types'>{chartTypes}</div>
                {lineChartsSvg}
            </div>
        </div>);
    }
}

function mapStateToProps(state) {
    return {
        currencies: state.currencies,
        chartTypes: state.chartTypes
    };
}

function mapDispatchToProps(dispatch) {
    return { stateActions: bindActionCreators(stateActions, dispatch) };
}

export default connect(mapStateToProps, mapDispatchToProps)(ChartPage);
