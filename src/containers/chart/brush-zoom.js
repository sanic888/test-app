import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import * as d3 from 'd3';

export class LineChartPage extends Component {
	constructor(props) {
		super(props);

		this.createLineChart = this.createLineChart.bind(this);
		this.stateBrush = {};
		// this.stateZoom = {};
	}

	static propTypes = {
		trades: PropTypes.object,
		currency: PropTypes.string
	};

	componentDidMount() {
		// this.prevLength = this.props.trades.sell.length;
		this.createLineChart(this.props.trades.sell, this.props.trades.buy);
	}

	_cleanSvg() {
		const nodes = this.node.childNodes, length = nodes.length;

		for (let i = length - 1; i > -1; i--) {
			nodes[i].parentNode.removeChild(nodes[i]);
		}
	}

	_cleanSvgV1(className) {
		const elems = document.getElementsByClassName(className);

		for (let i = 0; i < elems.length; i++) {
			elems[i].parentNode.removeChild(elems[i]);
		}
	}

	groupBy(source, key1, key2) {
		const res = {};

		source.forEach(s => {
			res[`${s[key1]}${s[key2]}`] = res[`${s[key1]}${s[key2]}`] || [];
			res[`${s[key1]}${s[key2]}`].push(s);
		});

		return res;
	}

	createLineChart(trades) {
		if (!this.node || !trades.length || !this.props.currency) return;

		this._cleanSvg();

		const svg = d3.select(this.node),
			margin = {top: 20, right: 20, bottom: 110, left: 40},
			margin2 = {top: 430, right: 20, bottom: 30, left: 40},
			width = +svg.attr("width") - margin.left - margin.right,
			height = +svg.attr("height") - margin.top - margin.bottom,
			height2 = +svg.attr("height") - margin2.top - margin2.bottom;

		const x = d3.scaleTime().range([0, width]),
			x2 = d3.scaleTime().range([0, width]),
			y = d3.scaleLinear().range([height, 0]),
			y2 = d3.scaleLinear().range([height2, 0]);

		const xAxis = d3.axisBottom(x),
			xAxis2 = d3.axisBottom(x2),
			yAxis = d3.axisLeft(y);

		const brush = d3.brushX()
			.extent([[0, 0], [width, height2]])
			.on("brush end", () => {
				if (d3.event.sourceEvent && d3.event.sourceEvent.type === "zoom") return; // ignore brush-by-zoom

				const s = d3.event.selection || x2.range();

				if (!renderInProgress || !this.stateBrush.scale || !this.stateBrush.translate) {
					this.stateBrush.scale = width / (s[1] - s[0]);
					this.stateBrush.translate = -s[0];
				}

				console.log(`${this.stateBrush.scale} ; ${this.stateBrush.translate}`);

				x.domain(s.map(x2.invert, x2));
				focus.select(".area").attr("d", area);
				focus.select(".axis--x").call(xAxis);
				svg.select(".zoom").call(zoom.transform, d3.zoomIdentity
					.scale(this.stateBrush.scale)
					.translate(this.stateBrush.translate, 0));
			});

		const zoom = d3.zoom()
			.scaleExtent([1, 10])
			.translateExtent([[0, 0], [width, height]])
			.extent([[5, 10], [width, height]])
			.on("zoom", () => {
				if (d3.event.sourceEvent && d3.event.sourceEvent.type === "brush") return; // ignore zoom-by-brush

				const t = d3.event.transform;

				x.domain(t.rescaleX(x2).domain());
				focus.select(".area").attr("d", area);
				focus.select(".axis--x").call(xAxis);
				context.select(".brush").call(brush.move, x.range().map(t.invertX, t));
			});

		const area = d3.area()
			.curve(d3.curveMonotoneX)
			.x(function(d) {
				return x(d.date);
			})
			.y0(height)
			.y1(function(d) {
				return y(d.price);
			});

		const area2 = d3.area()
			.curve(d3.curveMonotoneX)
			.x(function(d) {
				return x2(d.date);
			})
			.y0(height2)
			.y1(function(d) {
				return y2(d.price);
			});

		svg.append("defs").append("clipPath")
			.attr("id", "clip")
			.append("rect")
			.attr("width", width)
			.attr("height", height);

		const focus = svg.append("g")
			.attr("class", "focus")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		const context = svg.append("g")
			.attr("class", "context")
			.attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");


		const data = trades.filter(t => t.pair === this.props.currency).map(t => {
			return {
				date: new Date(t.date * 1000),
				price: +t.price,
				amount: t.amount,
				quantity: t.quantity
			};
		}).sort((a, b) => {
			if (a.date > b.date) {
				return 1;
			} else {
				return -1;
			}
		});

		x.domain(d3.extent(data, function(d) {
			return d.date;
		}));

		const minY = d3.min(data, function(d) {
			return d.price;
		});
		const maxY = d3.max(data, function(d) {
			return d.price;
		});
		const diffY = (maxY - minY) * 0.1;
		y.domain([minY - diffY, maxY + diffY]);
		x2.domain(x.domain());
		y2.domain(y.domain());

		focus.append("path")
			.datum(data)
			.attr("class", "area")
			.attr("d", area);

		focus.append("g")
			.attr("class", "axis axis--x")
			.attr("transform", "translate(0," + height + ")")
			.call(xAxis);

		focus.append("g")
			.attr("class", "axis axis--y")
			.call(yAxis);

		context.append("path")
			.datum(data)
			.attr("class", "area")
			.attr("d", area2);

		context.append("g")
			.attr("class", "axis axis--x")
			.attr("transform", "translate(0," + height2 + ")")
			.call(xAxis2);

		let renderInProgress = true;

		context.append("g")
			.attr("class", "brush")
			.call(brush)
			.call(brush.move, x.range());

		renderInProgress = false;

		svg.append("rect")
			.attr("class", "zoom")
			.attr("width", width)
			.attr("height", height)
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
			.call(zoom);

		console.log(`finish: ${this.stateBrush.scale} ; ${this.stateBrush.translate}`);

		if (this.stateBrush.scale && this.stateBrush.translate) {
			svg.select(".zoom").call(zoom.transform, d3.zoomIdentity
				.scale(this.stateBrush.scale)
				.translate(this.stateBrush.translate, 0));
		}
	}

	showInfo(infoTab, date, price) { // amount, quantity
		infoTab.select('.date').text(date);
		infoTab.select('.price').text(price);
	}

	getTimeString(date) {
		const formatTime = (d) => {
			return d < 10 ? `0${d}` : d;
		};

		return `${formatTime(date.getHours())}:${formatTime(date.getMinutes())}:${formatTime(date.getSeconds())}`;
	}

	render() {
		this.createLineChart(this.props.trades.sell, this.props.trades.buy);

		const date = new Date(),
			formatTime = (d) => {
				return d < 10 ? `0${d}` : d;
			},
			timeString = `${formatTime(date.getMonth() + 1)}/${formatTime(date.getDate())} - ${formatTime(date.getHours())}:${formatTime(date.getMinutes())}:${formatTime(date.getSeconds())}`;

		return (<div className='chart-wrapper'>
			<div className='chart-header'>
				<span className='chart-currency'>{this.props.currency}</span>
				<span className='chart-time'>{timeString}</span>
			</div>
			<svg ref={node => this.node = node} width={500} height={500}/>
		</div>);
	}
}

function mapStateToProps(state) {
	return {trades: state.trades.exmo};
}

function mapDispatchToProps() {
	return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(LineChartPage);
