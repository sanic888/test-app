import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import * as d3 from 'd3';

import i18n from '../../core/i18n';

export class LineChartSvgPage extends Component {
	constructor(props) {
		super(props);

		this.createLineChart = this.createLineChart.bind(this);
	}

	static propTypes = {
		trades: PropTypes.object,
		currency: PropTypes.string
	};

	componentDidMount() {
		this.createLineChart(this.props.trades.sell, this.props.trades.buy);
	}

	createLineChart(trades) {
		if (!this.node || !trades.length || !this.props.currency) return;

		const monthParse = d3.timeParse("%Y-%m");
		let months, monthKeys;

		const svg = d3.select("svg"),
			margin = {top: 20, right: 30, bottom: 30, left: 40},
			width = svg.attr("width") - margin.left - margin.right,
			height = svg.attr("height") - margin.top - margin.bottom,
			g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		const x = d3.scaleTime().range([0, width]), y = d3.scaleLinear().range([height, 0]);

		const voronoi = d3.voronoi()
			.x(function(d) {
				return x(d.date);
			})
			.y(function(d) {
				return y(d.value);
			})
			.extent([[-margin.left, -margin.top], [width + margin.right, height + margin.bottom]]);

		const line = d3.line()
			.x(function(d) {
				return x(d.date);
			})
			.y(function(d) {
				return y(d.value);
			});

		d3.tsv("unemployment.tsv", type, function(error, data) {
			if (error) throw error;

			x.domain(d3.extent(months));
			y.domain([0, d3.max(data, function(c) {
				return d3.max(c.values, function(d) {
					return d.value;
				});
			})]).nice();

			g.append("g")
				.attr("class", "axis axis--x")
				.attr("transform", "translate(0," + height + ")")
				.call(d3.axisBottom(x));

			g.append("g")
				.attr("class", "axis axis--y")
				.call(d3.axisLeft(y).ticks(10, "%"))
				.append("text")
				.attr("x", 4)
				.attr("y", 0.5)
				.attr("dy", "0.32em")
				.style("text-anchor", "start")
				.style("fill", "#000")
				.style("font-weight", "bold")
				.text(i18n.translate('PointLineChart.unemploymentRate'));

			g.append("g")
				.attr("class", "cities")
				.selectAll("path")
				.data(data)
				.enter().append("path")
				.attr("d", function(d) {
					d.line = this;

					return line(d.values);
				});

			const focus = g.append("g")
				.attr("transform", "translate(-100,-100)")
				.attr("class", "focus");

			focus.append("circle").attr("r", 3.5);
			focus.append("text").attr("y", -10);

			const voronoiGroup = g.append("g").attr("class", "voronoi");

			voronoiGroup.selectAll("path")
				.data(voronoi.polygons(d3.merge(data.map(function(d) {
					return d.values;
				}))))
				.enter().append("path")
				.attr("d", function(d) {
					return d ? "M" + d.join("L") + "Z" : null;
				})
				.on("mouseover", mouseover)
				.on("mouseout", mouseout);

			d3.select("#show-voronoi")
				.property("disabled", false)
				.on("change", function() {
					voronoiGroup.classed("voronoi--show", this.checked);
				});

			function mouseover(d) {
				d3.select(d.data.city.line).classed("city--hover", true);
				d.data.city.line.parentNode.appendChild(d.data.city.line);
				focus.attr("transform", "translate(" + x(d.data.date) + "," + y(d.data.value) + ")");
				focus.select("text").text(d.data.value);
			}

			function mouseout(d) {
				d3.select(d.data.city.line).classed("city--hover", false);
				focus.attr("transform", "translate(-100,-100)");
			}
		});

		function type(d, i, columns) {
			const c = {name: d.name, values: null};

			if (!months) {
				monthKeys = columns.slice(1);
				months = monthKeys.map(monthParse);

			}

			c.values = monthKeys.map(function(k, i) {
				return {city: c, date: months[i], value: d[k] / 100};
			});

			return c;
		}
	}

	render() {
		this.createLineChart(this.props.trades.sell, this.props.trades.buy);

		return (<div className='chart-wrapper'>
			<svg ref={node => this.node = node} width={500} height={500}/>
		</div>);
	}
}

const mapStateToProps = (state) => {
	return {trades: state.trades.exmo};
};

const mapDispatchToProps = () => {
	return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(LineChartSvgPage);
