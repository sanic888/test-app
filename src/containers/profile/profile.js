import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import restApi from '../../api/rest-api';
import NotificationActions from '../../actions/notification';
import Input from '../../components/controls/input/index';
import Button from '../../components/controls/button/index';
import i18n from '../../core/i18n';

const mapStateToProps = () => ({});
const mapDispatchToProps = dispatch => ({
    notificationActions: bindActionCreators(NotificationActions, dispatch)
});

@connect(mapStateToProps, mapDispatchToProps)
export default class Profile extends Component {
    static propTypes = {
        notificationActions: PropTypes.object
    };

    state = {
        enabledSave: false,
        email: '',
        userName: '',
        fullName: '',
        initialState: {
            userName: '',
            fullName: ''
        }
    };

    componentDidMount() {
        this.init();
    }

    init = () => {
        restApi.getProfile().then(user => {
            this.setState({
                email: user.email,
                userName: user.userName,
                fullName: user.fullName,
                initialState: {
                    userName: user.userName,
                    fullName: user.fullName
                }
            });
        });
    };

    validateEnabledSave = (userName, fullName) => {
        if (this.state.initialState.userName !== userName ||
            this.state.initialState.fullName !== fullName) {
            !this.state.enabledSave && this.setState({ enabledSave: true });
        } else {
            this.state.enabledSave && this.setState({ enabledSave: false });
        }
    };

    userNameChange = e => {
        this.setState({ userName: e.target.value });
        this.validateEnabledSave(e.target.value, this.state.fullName);
    };

    fullNameChange = e => {
        this.setState({ fullName: e.target.value });
        this.validateEnabledSave(this.state.userName, e.target.value);
    };

    save = event => {
        event.preventDefault();
        const userName = this.state.userName;
        const fullName = this.state.fullName;

        if (this.state.enabledSave) {
            restApi.updateProfile({ userName, fullName })
                .then(() => {
                    this.props.notificationActions.addSuccess(i18n.translate('Profile.successUpdatedMessage'));

                    this.setState({
                        initialState: { userName, fullName }
                    });

                    this.validateEnabledSave(userName, fullName);
                });
        }
    };

    generateColor = () => {
        const number1 = 255 / 26 * (this.state.email.charCodeAt(0) - 97);
        const number2 = 255 / 26 * (this.state.email.charCodeAt(1) - 97);

        return {
            color: `rgb(${number1}, ${number2}, 255)`,
            backgroundColor: `rgb(255, ${number1}, ${number2})`
        };
    };

    render() {
        const style = this.generateColor();
        return (
            <div className='es-profile-page'>
                <div className='es-header-container'>
                    <span className='es-avatar' style={style}>{i18n.translate('Profile.defaultAvatar')}</span>
                    <span className='es-info'>
						<span className='es-full-name'>{this.state.fullName}</span>
						<br/>
						<a href={`mailto:${this.state.email}`}>{this.state.email}</a>
					</span>
                </div>
                <form className='es-manage-controls' onSubmit={this.save}>
                    <Input
                        disabled
                        type='text'
                        placeholder={i18n.translate('Profile.emailInputPlaceholder')}
                        name='email'
                        defaultValue={this.state.email}/>
                    <Input
                        type='text'
                        placeholder={i18n.translate('Profile.userNameInputPlaceholder')}
                        name='userName'
                        onChange={this.userNameChange}
                        defaultValue={this.state.userName}/>
                    <Input
                        type='text'
                        placeholder={i18n.translate('Profile.fullNameInputPlaceholder')}
                        name='fullName'
                        onChange={this.fullNameChange}
                        defaultValue={this.state.fullName}/>

                    <div className='es-controls-container'>
                        <Button
                            text={i18n.translate('Profile.saveBtnLabel')}
                            type='submit'
                            className={this.state.enabledSave ? '' : 'disabled'}
                            onClick={this.save}/>
                    </div>
                </form>
            </div>
        );
    }
}
