import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import * as d3 from 'd3';

import CheckBox from '../../components/controls/checkbox/checkbox';
import RadioButton from '../../components/controls/radio-button/radio-button';
import { EXMO_ID } from '../../reducers/exchanges';

import './main-chart.scss';

const mapStateToProps = (state) => {
    return {
        trades: state.trades,
        currencies: state.currencies,
        exchanges: state.exchanges
    };
};

const mapDispatchToProps = () => {
    return {};
};

@connect(mapStateToProps, mapDispatchToProps)
export default class MainChartSvgPage extends Component {
    constructor(props) {
        super(props);

        const selectedExchangeType = EXMO_ID;
        const currencies = this.props.currencies;
        const selectedCurrency = this.props.currencies[0].name;
        const type = 'sell';

        this.state = {
            type,
            currencies,
            selectedExchangeType,
            selectedCurrency
        };
    }

    static propTypes = {
        trades: PropTypes.object,
        exchanges: PropTypes.object,
        currencies: PropTypes.array
    };
    
    componentDidMount() {
        // this.renderPieChart();
        // const currencies = this.state.currencies
        //     .filter(i => i.selected)
        //     .map(i => i.name);

        // this.change(this.props.tickers[this.state.selectedExchangeType].filter((item) => {
        //     return currencies.includes(item.pair);
        // }));
    }

    changeExchangeType = (selectedExchangeType) => {
        this.setState({ selectedExchangeType });
        this.selectedExchangeType = selectedExchangeType;

        const currencies = this.state.currencies
            .filter(i => i.selected)
            .map(i => i.name);

        // this.change(this.props.tickers[selectedExchangeType].filter((item) => {
        //     return currencies.includes(item.pair);
        // }));
    }

    changeCurrency = (selectedCurrency) => {
        this.setState({
            selectedCurrency
        });

        const trades = this.props.trades[this.state.selectedExchangeType][this.state.type]
            .filter(i => i.pair === selectedCurrency);

        trades.forEach(i => {
                i.DATE = new Date(i.date * 1000);
            });

        const preparedData = this.prepareData(trades);

        // this.change(this.props.tickers[this.state.selectedExchangeType].filter((item) => {
        //     return currencies.includes(item.pair);
        // }));
    }

    // return array of objects format
    // time - Date
    // min - Number
    // max - Number
    // open - Number
    // close - Number
    // increase - Boolean
    prepareData = (data) => {
        data.sort( (a, b) => b.date - a.date).forEach(i => {
            i.date = i.date * 1000;
        });

        const date = new Date();
        let current = date.getTime();
        const mins = date.getMinutes() % 30;
        const milSecIn30Mins = 1800000;
        let partes = Math.floor(date / milSecIn30Mins);
        let prev = partes * milSecIn30Mins;
        
        const result = [];
        let item = {
            date: new Date(prev)
        };

        data.forEach(i => {
            const price = +i.price;
            if(i.date <= current && i.date > prev) {
                if(!item.open) {
                    item.open = price;
                }
                if(!item.min) {
                    item.min = price;
                }
                if(!item.max) {
                    item.max = price;
                }

                if(item.min > price) {
                    item.min = price;
                }
                
                if(item.max < price) {
                    item.max = price;
                }

                item.close = +i.price;
            } else {
                item.increase = item.open > item.close;

                result.push(item);

                partes = partes - 1;
                current = prev;
                prev = partes * milSecIn30Mins;

                item = {
                    date: new Date(prev),
                    open: price,
                    min: price,
                    max: price,
                    close: price
                };
            }
        });

        console.log(result);
    }

    getExchangeTypes = () => {
        return this.props.exchanges.map((exchange) => {
            return <RadioButton key={exchange.id} text={exchange.name} selected={exchange.id === this.state.selectedExchangeType} onClick={() => this.changeExchangeType(exchange.id)} />;
        });
    }

    getCurrencies = () => {
        return this.state.currencies.map(currency => {
            return <RadioButton key={currency.name} text={currency.name} selected={currency.name === this.state.selectedCurrency} onClick={(state) => this.changeCurrency(currency.name)} />;
        });
    }

    render() {
        const exchangeTypes = this.getExchangeTypes();
        const currencies = this.getCurrencies();

        return (<div className='es-main-chart-wrapper'>
            <div className='es-price-statistics-header'>Price statistics for the last 24 hours</div>
            <div className='es-exchange-types-filter'>{exchangeTypes}</div>
            <div className='es-chart-hint'>At least one item should be selected</div>
            <div className='es-currency-filter'>{currencies}</div>
            <div ref={node => this.mainChartNode = node} width={600} height={400}>
            </div>
        </div>);
    }
}
