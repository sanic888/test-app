import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Bubble from './bubble';

export default class Game extends Component {
	constructor(props) {
		super(props);

		this.state = {
			top: 0,
			left: 0,
			right: 0,
			height: 0,
			width: 0
		};

		this.bubbleCount = 1;
	}

	static propTypes = {
		orientation: PropTypes.string
	};

	componentDidMount() {
		this.timerID = setInterval(
			() => {
				const header = document.getElementsByTagName('header'),
					main = document.getElementsByTagName('main'),
					styles = window.getComputedStyle(main[0]);

				if (header) {
					this.setState({
						top: header[0].clientHeight,
						width: styles.paddingLeft,
						height: styles.height
					});

					console.log('YEHHHHHHHH');

					clearInterval(this.timerID);
				}
			},
			1000
		);
	}

	componentWillUnmount() {
		clearInterval(this.timerID);
	}

	render() {
		const {orientation} = this.props,
			className = `game-${orientation}`,
			bubbles = [],
			style = {
				top: this.state.top,
				height: this.state.height,
				width: this.state.width
			};

		style[orientation] = 0;

		for (let i = 0; i < this.bubbleCount; i++) {
			bubbles.push(<Bubble key={i} width={this.state.width} height={this.state.height}/>);
		}

		return (<div className={className} style={style}>{bubbles}</div>);
	}
}
