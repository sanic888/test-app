import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Game extends Component {
	constructor(props) {
		super(props);

		this.boardMouseOver = this.boardMouseOver.bind(this);
		this.boardMouseOut = this.boardMouseOut.bind(this);
		this.boardMouseMove = this.boardMouseMove.bind(this);
		this.boardMouseDown = this.boardMouseDown.bind(this);
		this.boardMouseUp = this.boardMouseUp.bind(this);

		this.state = {
			left: 0,
			boardTop: 0
		};
		this.verticalDirection = 'down';
		this.horizontalDirection = 'right';
	}

	static propTypes = {
		height: PropTypes.string,
		width: PropTypes.string
	};

	componentDidMount() {
		this.timerID = setInterval(
			() => {
				const {height, width} = this.props;
				let left;

				if (this.top) {
					if (this.top > (+height.toString().replace('px', '') - 10)) {
						this.verticalDirection = 'up';
					}

					if (this.top < 2) {
						this.verticalDirection = 'down';
					}

					if (this.verticalDirection === 'up') {
						this.top = this.top - 1;
					} else if (this.verticalDirection === 'down') {
						this.top = this.top + 1;
					}
				}

				if (this.state.left > (+width.toString().replace('px', '') - 10)) {
					this.horizontalDirection = 'left';
				}

				if (this.state.left < 0) {
					this.horizontalDirection = 'right';
				}

				if (this.horizontalDirection === 'left') {
					left = this.state.left - 1;
				} else if (this.horizontalDirection === 'right') {
					left = this.state.left + 1;
				}

				this.setState({
					left,
					top: this.top,
				});
			},
			10
		);
	}

	componentWillUnmount() {
		clearInterval(this.timerID);
	}

	boardMouseOver() {
		this.isOn = true;
	}

	boardMouseOut() {
		this.isOn = false;
		this.isDown = false;
		this.startY = 0;
	}

	boardMouseMove(e) {
		if (this.isDown && this.isOn) {

			if (!this.startY) {
				this.startY = e.clientY;
			} else {
				const boardTop = e.clientY - this.startY;

				this.setState({boardTop: boardTop});
			}

			console.log('boardMouseMove');
		}
	}

	boardMouseDown() {
		this.isDown = true;
	}

	boardMouseUp() {
		this.isDown = false;
	}

	render() {
		const {height} = this.props;

		this.top = this.top || Math.ceil(Math.random() * +height.toString().replace('px', ''));

		const style = {
			left: this.state.left,
			top: this.top || 0
		};
		const boardStyle = {top: this.state.boardTop};

		return (
			<div>
				<div className='bubble' style={style}/>
				<div className='board'
				     style={boardStyle}
				     onMouseOver={this.boardMouseOver}
				     onMouseOut={this.boardMouseOut}
				     onMouseMove={this.boardMouseMove}
				     onMouseDown={this.boardMouseDown}
				     onMouseUp={this.boardMouseUp}/>
			</div>
		);
	}
}
