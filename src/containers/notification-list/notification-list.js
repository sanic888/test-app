import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Notification from './../../components/notification/notification';
import notificationActions from '../../actions/notification';

const mapStateToProps = state => ({ notifications: state.notifications });
const mapDispatchToProps = dispatch => ({ actions: bindActionCreators(notificationActions, dispatch) });

@connect(mapStateToProps, mapDispatchToProps)
export default class NotificationList extends Component {
    onCloseBtnClick(id) {
        this.props.actions.remove(id);
    }

    render() {
        return (
            <div className='es-notification-list'>
                {this.props.notifications.map(n =>
                    <Notification key={n.id} {...n} onCloseBtnClick={() => this.onCloseBtnClick(n.id)}/>
                )}
            </div>
        );
    }
}
