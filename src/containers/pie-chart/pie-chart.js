import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import * as d3 from 'd3';

import CheckBox from '../../components/controls/checkbox/checkbox';
import RadioButton from '../../components/controls/radio-button/radio-button';
import { EXMO_ID } from '../../reducers/exchanges';

import './pie-chart.scss';

const mapStateToProps = (state) => {
    return {
        tickers: state.tickers,
        currencies: state.currencies,
        exchanges: state.exchanges
    };
};

const mapDispatchToProps = () => {
    return {};
};

@connect(mapStateToProps, mapDispatchToProps)
export default class PieChartSvgPage extends Component {
    constructor(props) {
        super(props);

        const selectedExchangeType = EXMO_ID;
        const currencies = this.props.currencies;

        this.state = {
            currencies,
            selectedExchangeType,
            selectedType: 'vol',
            pieTypes: [
                { id: 'vol', name: 'Volume of all trades for the last 24 hours' },
                { id: 'vol_curr', name: 'Sum of all trades for the last 24 hours' }
            ]
        };
    }

    static propTypes = {
        tickers: PropTypes.object,
        exchanges: PropTypes.object,
        currencies: PropTypes.array
    };

    componentDidMount() {
        this.renderPieChart();
        const currencies = this.state.currencies
            .filter(i => i.selected)
            .map(i => i.name);

        this.change(this.props.tickers[this.state.selectedExchangeType].filter((item) => {
            return currencies.includes(item.pair);
        }));
    }

    componentDidUpdate() {
        
    }

    renderPieChart = () => {
        if (!this.pieChartNode) return;

        const width = 600;
        const height = 400;
        const leftMargin = 5;

        this.radius = Math.min(width, height) / 2;

        this.color = d3.scaleOrdinal(d3.schemeCategory20.map((color) => {
            return d3.interpolateRgb(color, '#000')(0.2);
        }));

        this.selectedType = this.state.selectedType;
        this.pie = d3.pie()
            .value((d) => {
                return d[this.selectedType];
            })
            .sort(null);

        this.arc = d3.arc()
            .innerRadius(0)
            .outerRadius(this.radius - 20);

        this.svg = d3.select(this.pieChartNode).append('svg')
            .attr('width', width)
            .attr('height', height)
            .append('g')
            .attr('transform', `translate(${Math.min(width, height) / 2 + leftMargin},${height / 2})`);

        this.rendered = true;
    }

    change = (values) => {
        if (!values || !values.length || !this.rendered) {
            return;
        }
        console.dir(values);

        // Calculate sum
        let volCurrSum = 0;
        let volSum = 0;
        values.forEach(item => {
            volSum += +item.vol;
            volCurrSum += +item.vol_curr;
        });
        // Set percentage
        values.forEach(item => {
            item.vol_sum = volSum;
            item.vol_curr_sum = volCurrSum;

            item.vol_per = (+item.vol / volSum * 100);
            item.vol_curr_per = (+item.vol_curr / volCurrSum * 100);
        });

        const svg = this.svg;
        const pie = this.pie;
        const arc = this.arc;
        const color = this.color;
        const radius = this.radius;
        const duration = 100;

        let path = svg.selectAll('path');

        const key = (d) => {
            console.log(d);
            return d.data.pair;
        };

        function arcTween(d) {
            const i = d3.interpolate(this._current, d);
            this._current = i(1);

            return (t) => {
                return arc(i(t));
            };
        }

        const mousemove = () => {
            const e = window.event;

            this.pieChartTooltip.style.top = e.offsetY - this.pieChartTooltip.offsetHeight + 'px';
            this.pieChartTooltip.style.left = e.offsetX + 10 + 'px';
        };

        const data0 = path.data(),
            data1 = pie(values);

        // JOIN elements with new data.
        path = path.data(data1, (d) => {
            console.log(d);
            return d.data.pair;
        });

        // EXIT old elements from the screen.
        path.exit()
            .datum((d, i) => {
                return this.findNeighborArc(i, data1, data0, key) || d;
            })
            .transition()
            .duration(duration)
            .attrTween('d', arcTween)
            .remove();

        // ENTER new elements in the array.
        const self = this;
        path.enter()
            .append('path')
            .each((d, i) => {
                this._current = self.findNeighborArc(i, data0, data1, key) || d;
            })
            .attr('fill', (d) => { return color(d.data.pair); })
            .merge(path)
            .on('mouseover', this.gArcOnmousemove(5, 0, 'block'))
            .on('mouseout', this.gArcOnmousemove(0, 100, 'none'))
            .on('mousemove', mousemove)
            .transition()
            .duration(duration)
            .attrTween('d', arcTween);

        //* ************* ADD description **************
        const circle = svg.selectAll('circle').data(values);
        const circleRadius = 7;
        // EXIT old elements from the screen.
        circle.exit().remove();
        // ENTER new elements in the array.
        circle.enter()
            .append('circle')
            .attr('r', circleRadius)
            // Merge items are should be UPDATED
            .merge(circle)
            .attr('class', (d) => {
                return `es-svg-info-circle ${d.pair}`;
            })
            .attr('cx', radius)
            .attr('cy', (d, i) => {
                return i * 20 - radius + 20;
            })
            .attr('fill', (d) => {
                return color(d.pair);
            });

        // Add text for the description
        const text = svg.selectAll('text').data(values);
        // EXIT old elements from the screen.
        text.exit().remove();

        // ENTER new elements in the array.
        text.enter()
            .append('text')
            .attr('font-size', '10px')
            // Merge items are should be UPDATED
            .merge(text)
            .attr('class', (d) => {
                return `es-svg-info-text ${d.pair}`;
            })
            .attr('x', radius + 15)
            .attr('y', (d, i) => {
                return i * 20 - radius + 23;
            })
            .text((d) => {
                const val = parseFloat(d[this.selectedType]).toFixed(2);
                const per = parseFloat(d[`${this.selectedType}_per`]).toFixed(2);
                return `${d.pair} - ${val}(${per}%)`;
            });

        //* ************* END description **************
    }

    gArcOnmousemove = (x, delay, display) => {
        return (d) => {
            d3.select(this.pieChartNode.querySelector(`.es-svg-info-circle.${d.data.pair}`))
                .transition(100)
                .delay(delay)
                .attr('transform', () => {
                    return 'translate(' + x + ',' + 0 + ')';
                });
            d3.select(this.pieChartNode.querySelector(`.es-svg-info-text.${d.data.pair}`))
                .transition(100)
                .delay(delay)
                .attr('transform', () => {
                    return 'translate(' + x + ',' + 0 + ')';
                });

            if (this.pieChartTooltip.style.display !== display) {
                this.pieChartTooltip.style.display = display;
            }

            // Set information for focus popup
            this.pieChartTooltipPair.textContent = d.data.pair;
            this.pieChartTooltipVol.textContent = parseFloat(d.data.vol).toFixed(2);
            this.pieChartTooltipVolCurr.textContent = parseFloat(d.data.vol_curr).toFixed(2);
        };
    }

    findFollowing = (i, data0, data1, key) => {
        const n = data1.length;
        const m = data0.length;

        while (++i < n) {
            const k = key(data1[i]);
            for (let j = 0; j < m; ++j) {
                if (key(data0[j]) === k) {
                    return data0[j];
                }
            }
        }
    }

    findPreceding = (i, data0, data1, key) => {
        const m = data0.length;

        while (--i >= 0) {
            const k = key(data1[i]);
            for (let j = 0; j < m; ++j) {
                if (key(data0[j]) === k) {
                    return data0[j];
                }
            }
        }
    }

    findNeighborArc = (i, data0, data1, key) => {
        let d;
        return (d = this.findPreceding(i, data0, data1, key)) ? { startAngle: d.endAngle, endAngle: d.endAngle } // eslint-disable-line no-cond-assign
            : (d = this.findFollowing(i, data0, data1, key)) ? { startAngle: d.startAngle, endAngle: d.startAngle } // eslint-disable-line no-cond-assign
                : null;
    }

    changeType = (selectedType) => {
        this.setState({ selectedType });
        this.selectedType = selectedType;

        const currencies = this.state.currencies
            .filter(i => i.selected)
            .map(i => i.name);

        this.change(this.props.tickers[this.state.selectedExchangeType].filter((item) => {
            return currencies.includes(item.pair);
        }));
    }

    changeExchangeType = (selectedExchangeType) => {
        this.setState({ selectedExchangeType });
        this.selectedExchangeType = selectedExchangeType;

        const currencies = this.state.currencies
            .filter(i => i.selected)
            .map(i => i.name);

        this.change(this.props.tickers[selectedExchangeType].filter((item) => {
            return currencies.includes(item.pair);
        }));
    }

    minNumberOfSelectedCurrenices = 1;

    toggleCurrency = (currency, state) => {
        const numberOfSelected = this.state.currencies
            .filter(i => i.selected).length;

        if (numberOfSelected > this.minNumberOfSelectedCurrenices || state) {
            this.setState({
                currencies: this.state.currencies.map((curr) => {
                    if (curr.name === currency) {
                        curr.selected = state;
                    }

                    return curr;
                })
            });

            const currencies = this.state.currencies
                .filter(i => i.selected)
                .map(i => i.name);

            this.change(this.props.tickers[this.state.selectedExchangeType].filter((item) => {
                return currencies.includes(item.pair);
            }));

            return state;
        }

        return true;
    }

    componentWillReceiveProps = (nextProps) => {
        const _currencies = this.state.currencies
            .filter(i => i.selected)
            .map(i => i.name);

        this.change(nextProps.tickers[this.state.selectedExchangeType].filter((item) => {
            return _currencies.includes(item.pair);
        }));
    }

    getTypes = () => {
        return this.state.pieTypes.map((type) => {
            return <RadioButton key={type.id} text={type.name} selected={type.id === this.state.selectedType} onClick={() => this.changeType(type.id)} />;
        });
    }

    getExchangeTypes = () => {
        return this.props.exchanges.map((exchange) => {
            return <RadioButton key={exchange.id} text={exchange.name} selected={exchange.id === this.state.selectedExchangeType} onClick={() => this.changeExchangeType(exchange.id)} />;
        });
    }

    getCurrencies = () => {
        return this.state.currencies.map(currency => {
            return <CheckBox key={currency.name} text={currency.name} selected={currency.selected} onClick={(state) => this.toggleCurrency(currency.name, state)} />;
        });
    }

    render() {
        const types = this.getTypes();
        const exchangeTypes = this.getExchangeTypes();
        const currencies = this.getCurrencies();
        const pieChartTooltipStyles = {
            borderRadius: '5px',
            backgroundColor: 'rgba(206, 206, 206, 0.5)',
            padding: '10px',
            position: 'absolute',
            display: 'none',
            top: 0,
            left: 0
        };
        const pieChartStyles = {
            position: 'relative'
        };

        return (<div className='es-pie-chart-wrapper'>
            <div className='es-price-statistics-header'>Price statistics for the last 24 hours</div>
            <div className='es-exchange-types-filter'>{exchangeTypes}</div>
            <div className='es-chart-types-filter'>{types}</div>
            <div className='es-chart-hint'>At least one item should be selected</div>
            <div className='es-currency-filter'>{currencies}</div>
            <div style={pieChartStyles} ref={node => this.pieChartNode = node} width={600} height={400}>
                <div style={pieChartTooltipStyles} ref={node => this.pieChartTooltip = node}>
                    <div ref={node => this.pieChartTooltipPair = node}></div>
                    Volume: <span ref={node => this.pieChartTooltipVol = node}></span><br />
                    Volume Curr: <span ref={node => this.pieChartTooltipVolCurr = node}></span>
                </div>
            </div>
        </div>);
    }
}
