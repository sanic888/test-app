import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, NavLink, Switch, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import Clock from '../../components/clock/clock';
import NotificationList from '../notification-list/notification-list';
import restApi from '../../api/rest-api';
import webSocketApi from '../../api/webSocket-api';
import Authentication from '../login/authentication';
import Info from '../../components/info';
import SetResetPassword from '../login/popups/set-reset-password';
import Currency from '../currency/currency';
import Chart from '../chart/chart';
import Profile from '../profile/profile';
import ConfirmRegister from '../login/popups/confirm';
import ProtectedRoute from '../../components/protected-route';
import PieChart from '../pie-chart/pie-chart';
import MainChart from '../main-chart/main-chart';

import i18n from '../../core/i18n';

const mapStateToProps = state => ({ user: state.user });

@connect(mapStateToProps)
export default class App extends Component {
    static propTypes = {
        user: PropTypes.shape({
            isAuthenticated: PropTypes.bool.isRequired
        })
    };

    state = {
        loaded: false,
        showLoginFlow: ''
    };

    componentDidMount() {
        this.initializing();
    }

    initializing() {
        webSocketApi.subscribe();

        Promise
            .all([
                i18n.initialize(),
                restApi.getTrades(),
                restApi.getCurrencies(),
                restApi.getOrdersBooks(),
                restApi.getTickers()
            ])
            .then(() => this.setState({ loaded: true }));
    }

    render() {
        const isAuthenticated = this.props.user.isAuthenticated;
        let resultHtml =
            (<div className='container'>
                <div className='lds-circle'/>
            </div>);

        if (this.state.loaded) {
            resultHtml =
                (<div className='container'>
                    <header>
                        <div className='menu'>
                            <NavLink to='/chart' className='link' activeClassName='active'>
                                {i18n.translate('App.chartsNavLink')}
                            </NavLink>

                            <NavLink to='/currency' className='link' activeClassName='active'>
                                {i18n.translate('App.currenciesNavLink')}
                            </NavLink>

                            {isAuthenticated &&
                                <NavLink to='/profile' className='link' activeClassName='active'>
                                    {i18n.translate('App.profileNavLink')}
                                </NavLink>
                            }

                            <NavLink to='/pie-chart' className='link' activeClassName='active'>Pie Charts</NavLink>

                            <NavLink to='/main-chart' className='link' activeClassName='active'>Main Charts</NavLink>

                            <NavLink to='/info' className='link' activeClassName='active'>
                                {i18n.translate('App.infoNavLink')}
                            </NavLink>

                            <Authentication/>

                            <Clock/>
                        </div>
                    </header>

                    <main>
                        <Switch>
                            <Route path='/info' component={Info}/>
                            <Route path='/chart' component={Chart}/>
                            <Route path='/currency' component={Currency}/>
                            <Route path='/pie-chart' component={PieChart}/>
                            <Route path='/main-chart' component={MainChart}/>
                            <Route path='/confirm-register/:token' component={ConfirmRegister}/>
                            <Route path='/reset-password/:token' component={SetResetPassword}/>

                            <ProtectedRoute path='/profile' component={Profile}/>

                            <Redirect exact from='*' to='/chart'/>
                        </Switch>
                    </main>

                    <footer>
                        {i18n.translate('App.footer')}
                    </footer>

                    <NotificationList/>
                </div>);
        }

        return (resultHtml);
    }
}
