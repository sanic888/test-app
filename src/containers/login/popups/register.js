import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// import { Link } from 'react-router';
import PropTypes from 'prop-types';

import sessionLoginFlowApi from '../../../api/session-login-flow';
import NotificationActions from '../../../actions/notification';
import StateActions from '../../../actions/state';
import Input from '../../../components/controls/input/index';
import Button from '../../../components/controls/button/index';
import { EMAIL } from '../../../constants/regexp';
import i18n from '../../../core/i18n';

const mapStateToProps = () => ({});
const mapDispatchToProps = dispatch => ({
    notificationActions: bindActionCreators(NotificationActions, dispatch),
    stateActions: bindActionCreators(StateActions, dispatch)
});

@connect(mapStateToProps, mapDispatchToProps)
export default class SignUpPage extends Component {
    static propTypes = {
        stateActions: PropTypes.object,
        notificationActions: PropTypes.object,
        onCloseApp: PropTypes.func
    };

    state = { enabledSignUp: false };

    validateEmail = email => EMAIL.test(email);

    validateEnabledSignUp = email => {
        if (this.validateEmail(email)) {
            !this.state.enabledSignUp && this.setState({ enabledSignUp: true });
        } else {
            this.state.enabledSignUp && this.setState({ enabledSignUp: false });
        }
    };

    emailChange = e => {
        this.setState({ email: e.target.value });
        this.validateEnabledSignUp(e.target.value);
    };

    userNameChange = e => this.setState({ userName: e.target.value });

    fullNameChange = e => this.setState({ fullName: e.target.value });

    signUp = event => {
        event.preventDefault();

        if (this.state.enabledSignUp) {
            sessionLoginFlowApi.signUp({
                email: this.state.email,
                userName: this.state.userName,
                fullName: this.state.fullName
            })
                .then(res => {
                    this.props.notificationActions.addSuccess(res.message);
                    this.props.stateActions.changeLoginFlow('');
                });
        }
    };

    onClosePopup = () => this.props.stateActions.changeLoginFlow('');

    render() {
        return (
            <div>
                <div className='popup-background'>
                </div>
                <div className='popup'>
                    <form className='popup-content' onSubmit={this.signUp}>
                        <div className='popup-header'>
                            <span className='inactive'>{i18n.translate('RegisterModal.headerText')}</span>
                            <span className='close' onClick={this.onClosePopup}/>
                        </div>
                        <Input
                            autoFocus
                            required
                            type='text'
                            placeholder={i18n.translate('RegisterModal.emailInputPlaceholder')}
                            name='email'
                            onChange={this.emailChange}/>
                        <Input
                            type='text'
                            placeholder={i18n.translate('RegisterModal.userNameInputPlaceholder')}
                            name='userName'
                            onChange={this.userNameChange}/>
                        <Input
                            type='text'
                            placeholder={i18n.translate('RegisterModal.fullNameInputPlaceholder')}
                            name='fullName'
                            onChange={this.fullNameChange}/>
                        <div className='popup-footer'>
                            <Button
                                text={i18n.translate('RegisterModal.submitBtnLabel')}
                                type='submit'
                                className={this.state.enabledSignUp ? '' : 'disabled'}
                                onClick={this.signUp}/>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}
