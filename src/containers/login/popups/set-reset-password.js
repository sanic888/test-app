import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import sessionLoginFlowApi from '../../../api/session-login-flow';
import Input from '../../../components/controls/input/index';
import Button from '../../../components/controls/button/index';
import i18n from '../../../core/i18n';

export class SetPassword extends Component {
    constructor(props) {
        super(props);

        this.setResetPassword = this.setResetPassword.bind(this);
        this.passwordChange = this.passwordChange.bind(this);
        this.confirmPasswordChange = this.confirmPasswordChange.bind(this);

        this.state = { enabledRegister: false };
    }

    static propTypes = {
        match: PropTypes.object
    };

    validateEnabledSignUp(password, confirmPassword) {
        if (password && confirmPassword && password === confirmPassword) {
            !this.state.enabledRegister && this.setState({ enabledRegister: true });
        } else {
            this.state.enabledRegister && this.setState({ enabledRegister: false });
        }
    }

    passwordChange(e) {
        this.setState({ password: e.target.value });
        this.validateEnabledSignUp(e.target.value, this.state.confirmPassword);
    }

    confirmPasswordChange(e) {
        this.setState({ confirmPassword: e.target.value });
        this.validateEnabledSignUp(this.state.password, e.target.value);
    }

    setResetPassword(event) {
        event.preventDefault();

        this.state.enabledRegister && sessionLoginFlowApi.setResetPassword({
            password: this.state.password,
            token: this.props.match.params.token
        });
    }

    render() {
        return (
            <div>
                <div className='popup-background'>
                </div>
                <div className='popup'>
                    <form className='popup-content' onSubmit={this.register}>
                        <div className='popup-header'>
                            <span className='inactive'>{i18n.translate('SetPassModal.headerText')}</span>
                        </div>

                        <Input
                            type='password'
                            onChange={this.passwordChange}
                            placeholder={i18n.translate('SetPassModal.passInputPlaceholder')}
                            autoFocus
                        />
                        <Input
                            type='password'
                            onChange={this.confirmPasswordChange}
                            placeholder={i18n.translate('SetPassModal.confirmPassInputPlaceholder')}/>

                        <div className='popup-footer'>
                            <Button
                                type='submit'
                                className={this.state.enabledRegister ? '' : 'disabled'}
                                onClick={this.setResetPassword}
                                text={i18n.translate('SetPassModal.submitBtnLabel')}
                            />
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

function mapStateToProps() {
    return {};
}

function mapDispatchToProps() {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(SetPassword);
