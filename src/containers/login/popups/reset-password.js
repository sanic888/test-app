import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import sessionLoginFlowApi from '../../../api/session-login-flow';
import StateActions from '../../../actions/state';
import Input from '../../../components/controls/input/index';
import Button from '../../../components/controls/button/index';
import { EMAIL } from '../../../constants/regexp';
import i18n from '../../../core/i18n';

export class ResetPassword extends Component {
    constructor(props) {
        super(props);

        this.emailChange = this.emailChange.bind(this);
        this.sendPasswordReset = this.sendPasswordReset.bind(this);
        this.onClosePopup = this.onClosePopup.bind(this);
        this.goToLogin = this.goToLogin.bind(this);

        this.state = { enabledSendPasswordReset: false };
    }

    static propTypes = {
        stateActions: PropTypes.object,
        params: PropTypes.object
    };

    validateEmail(email) {
        return EMAIL.test(email);
    }

    validateEnabledEmail(email) {
        if (this.validateEmail(email)) {
            !this.state.enabledSendPasswordReset && this.setState({ enabledSendPasswordReset: true });
        } else {
            this.state.enabledSendPasswordReset && this.setState({ enabledSendPasswordReset: false });
        }
    }

    emailChange(e) {
        this.setState({ email: e.target.value });
        this.validateEnabledEmail(e.target.value);
    }

    sendPasswordReset(event) {
        event.preventDefault();

        this.state.enabledSendPasswordReset && sessionLoginFlowApi.resetPassword({
            email: this.state.email
        });
    }

    onClosePopup() {
        this.props.stateActions.changeLoginFlow('');
    }

    goToLogin() {
        this.props.stateActions.changeLoginFlow('login');
    }

    render() {
        return (
            <div>
                <div className='popup-background'>
                </div>
                <div className='popup'>
                    <form className='popup-content' onSubmit={this.sendPasswordReset}>
                        <div className='popup-header'>
                            <span className='inactive'>{i18n.translate('ResetPassModal.headerText')}</span>
                            <span className='close' onClick={this.onClosePopup}/>
                        </div>

                        <p>{i18n.translate('ResetPassModal.bodyText')}</p>
                        <Input
                            type='email'
                            name='email'
                            onChange={this.emailChange}
                            placeholder={i18n.translate('ResetPassModal.emailInputPlaceholder')}
                            autoFocus/>

                        <div className='popup-footer'>
                            <Button
                                type='submit'
                                className={this.state.enabledSendPasswordReset ? '' : 'disabled'}
                                onClick={this.sendPasswordReset}
                                text={i18n.translate('ResetPassModal.submitBtnLabel')}/>
                            <a onClick={this.goToLogin} className='a-link'>
                                {i18n.translate('ResetPassModal.loginLabel')}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

function mapStateToProps() {
    return {};
}

function mapDispatchToProps(dispatch) {
    return {
        stateActions: bindActionCreators(StateActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);
