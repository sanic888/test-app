import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import sessionLoginFlowApi from '../../../api/session-login-flow';
import stateActions, { RESET_PASSWORD } from '../../../actions/state';
import Input from '../../../components/controls/input/index';
import Button from '../../../components/controls/button/index';
import { EMAIL } from '../../../constants/regexp';
import i18n from '../../../core/i18n';

const mapDispatchToProps = dispatch => ({ stateActions: bindActionCreators(stateActions, dispatch) });

@connect(null, mapDispatchToProps)
export default class LoginPage extends Component {
    static propTypes = {
        stateActions: PropTypes.shape({
            changeLoginFlow: PropTypes.func.isRequired,
            changeSelectedChartType: PropTypes.func.isRequired,
            toggleCurrency: PropTypes.func.isRequired
        })
    };

    state = { enabledLogin: false };

    onEmailChange = event => {
        const email = event.target.value;

        this.setState({ email });
        this.validateEnabledLogin(email, this.state.password);
    };

    onPasswordChange = event => {
        const password = event.target.value;

        this.setState({ password });
        this.validateEnabledLogin(this.state.email, password);
    };

    onSignIn = event => {
        const { email, password } = this.state;
        event.preventDefault();

        if (this.state.enabledLogin) {
            sessionLoginFlowApi.signIn({
                username: email,
                password: password
            });
        }
    };

    onForgotPassword = () => this.props.stateActions.changeLoginFlow(RESET_PASSWORD);

    onClosePopup = () => this.props.stateActions.changeLoginFlow(null);

    validateEnabledLogin(email, password) {
        function validateEmail() {
            return EMAIL.test(email);
        }

        if (validateEmail() && password) {
            if (!this.state.enabledLogin) {
                this.setState({ enabledLogin: true });
            }
        } else if (this.state.enabledLogin) {
            this.setState({ enabledLogin: false });
        }
    }

    render() {
        return (
            <div className='es-login-modal-container'>
                <div className='popup-background'/>
                <div className='popup'>
                    <form className='popup-content' onSubmit={this.signIn}>
                        <div className='popup-header'>
                            <span className='inactive'>{i18n.translate('LoginModal.headerText')}</span>
                            <span className='close' onClick={this.onClosePopup}/>
                        </div>

                        <Input
                            type='text'
                            name='email'
                            onChange={this.onEmailChange}
                            placeholder={i18n.translate('LoginModal.emailInputPlaceholder')}
                            autoFocus/>
                        <Input
                            type='password'
                            onChange={this.onPasswordChange}
                            placeholder={i18n.translate('LoginModal.passInputPlaceholder')}/>

                        <div className='popup-footer'>
                            <Button
                                type='submit'
                                className={this.state.enabledLogin ? '' : 'disabled'}
                                onClick={this.onSignIn}
                                text={i18n.translate('LoginModal.submitBtnLabel')}/>
                            <a onClick={this.onForgotPassword} className='a-link'>
                                {i18n.translate('LoginModal.forgetPassLabel')}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}
