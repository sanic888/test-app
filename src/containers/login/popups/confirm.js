import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import sessionLoginFlowApi from '../../../api/session-login-flow';
import Input from '../../../components/controls/input/index';
import Button from '../../../components/controls/button/index';
import i18n from '../../../core/i18n';

export class Confirm extends Component {
    constructor(props) {
        super(props);

        this.register = this.register.bind(this);
        this.passwordChange = this.passwordChange.bind(this);
        this.confirmPasswordChange = this.confirmPasswordChange.bind(this);

        this.state = { enabledRegister: false };
    }

    static propTypes = {
        match: PropTypes.object
    };

    validateEnabledSignUp(password, confirmPassword) {
        if (password && confirmPassword && password === confirmPassword) {
            !this.state.enabledRegister && this.setState({ enabledRegister: true });
        } else {
            this.state.enabledRegister && this.setState({ enabledRegister: false });
        }
    }

    passwordChange(e) {
        this.setState({ password: e.target.value });
        this.validateEnabledSignUp(e.target.value, this.state.confirmPassword);
    }

    confirmPasswordChange(e) {
        this.setState({ confirmPassword: e.target.value });
        this.validateEnabledSignUp(this.state.password, e.target.value);
    }

    register(event) {
        event.preventDefault();

        this.state.enabledRegister && sessionLoginFlowApi.register({
            password: this.state.password,
            token: this.props.match.params.token
        });
    }

    render() {
        return (
            <div>
                <div className='popup-background'>
                </div>
                <div className='popup'>
                    <form className='popup-content' onSubmit={this.register}>
                        <div className='popup-header'>
                            <span className='inactive'>{i18n.translate('ConfirmModal.headerText')}</span>
                        </div>

                        <Input
                            type='password'
                            onChange={this.passwordChange}
                            placeholder={i18n.translate('ConfirmModal.passInputPlaceholder')}
                            autoFocus/>
                        <Input
                            type='password'
                            onChange={this.confirmPasswordChange}
                            placeholder={i18n.translate('ConfirmModal.confirmPassInputPlaceholder')}/>

                        <div className='popup-footer'>
                            <Button
                                type='submit'
                                onClick={this.register}
                                className={this.state.enabledRegister ? '' : 'disabled'}
                                text={i18n.translate('ConfirmModal.submitBtnLabel')}/>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

function mapStateToProps() {
    return {};
}

function mapDispatchToProps() {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(Confirm);
