import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import stateActions, { REGISTER, LOGIN, RESET_PASSWORD } from '../../actions/state';
import sessionLoginFlowApi from '../../api/session-login-flow';
import LoginPage from './../login/popups/login';
import Register from './../login/popups/register';
import ResetPassword from './../login/popups/reset-password';
import i18n from '../../core/i18n';

const mapStateToProps = state => ({ user: state.user, loginFlow: state.loginFlow }),
      mapDispatchToProps = dispatch => ({ stateActions: bindActionCreators(stateActions, dispatch) });

@connect(mapStateToProps, mapDispatchToProps)
export default class Authentication extends Component {
    static propTypes = {
        stateActions: PropTypes.shape({
            changeLoginFlow: PropTypes.func.isRequired,
            changeSelectedChartType: PropTypes.func.isRequired,
            toggleCurrency: PropTypes.func.isRequired
        }),
        user: PropTypes.shape({ isAuthenticated: PropTypes.bool.isRequired }),
        loginFlow: PropTypes.string
    };

    onRegister = () => this.props.stateActions.changeLoginFlow(REGISTER);

    onLogin = () => this.props.stateActions.changeLoginFlow(LOGIN);

    onLogout = () => {
        sessionLoginFlowApi.logout().then(() => this.props.stateActions.changeLoginFlow(null));
    };

    render() {
        let login, loginContainer;

        if (this.props.user.isAuthenticated) {
            loginContainer =
                (<div className='es-login-container'>
                    <a onClick={this.onLogout}>{i18n.translate('Authentication.logout')}</a>
                </div>);
        } else {
            switch (this.props.loginFlow) {
                case REGISTER:
                    login = <Register/>;
                    break;
                case LOGIN:
                    login = <LoginPage/>;
                    break;
                case RESET_PASSWORD:
                    login = <ResetPassword/>;
                    break;
                default:
                    login = null;
            }

            loginContainer =
                (<div className='es-login-container'>
                    <a onClick={this.onRegister}>{i18n.translate('Authentication.register')}</a>
                    &brvbar;
                    <a onClick={this.onLogin}>{i18n.translate('Authentication.login')}</a>
                </div>);
        }

        return (<div className={'es-authentication-container'}>{loginContainer} {login}</div>);
    }
}
