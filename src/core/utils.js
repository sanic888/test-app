export default class Utils {
	static guidGenerator() {
		const S4 = () => Math.round((1 + Math.random()) * 0x10000).toString(16).substring(1);
		return (`${S4() + S4()}-${S4()}-${S4()}-${S4()}-${S4()}${S4()}${S4()}`);
	}
}
