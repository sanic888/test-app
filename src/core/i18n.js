import restApi from '../api/rest-api';

class I18n {
    constructor() {
        this.defaultLang = 'en-Us';
        this.language = null;
        this.locale = null;
    }

    translate(id, params) {
        const unpreparedMsg = this.locale[id];

        if(!unpreparedMsg) {
            return id;
        }else {
            return params ? this.substituteParameters(unpreparedMsg, params) : unpreparedMsg;
        }
    }

    initialize() {
        this.setCurrentLang();
        restApi.getLocale(this.getCurrentLangWithoutRegion()).then(locale => this.locale = locale);
    }

    setCurrentLang() {
        if(process.env.NODE_ENV === 'development'){
            this.language = this.defaultLang;
        } else {
            this.language = (navigator.languages && navigator.languages[0]) || navigator.language || this.defaultLang;
        }
    }

    getCurrentLang() {
        return this.language;
    }

    getCurrentLangWithoutRegion() {
        return this.getCurrentLang().toLowerCase().split(/[_-]+/)[0];
    }

    substituteParameters(message, params) {
        let param, reg, res;

        for (param in params) {
            if (params.hasOwnProperty(param)) {
                reg = new RegExp(`{{${param}}}`, 'ig');
                res = message.replace(reg, params[param]);
            }
        }

        return res;
    }
}

export default new I18n();
