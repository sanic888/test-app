# stats

-- node version 8.11.3
-- npm version 5.6.0

1. npm i
2. start mongo instance (setup connection string in server/config.json), for instance: mongod --dbpath=d:/db
3. npm run server
4. npm run dev
5. open http://localhost:3003

https://exmo.com/ru/api
https://bittrex.github.io/api/v1-1