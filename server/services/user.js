const mongo = require('../mongo');
const mongoskin = require('mongoskin');
const MongoDataService = require('../infrastructure/mongoDataService');
const user = new MongoDataService(mongo.User);
const bcrypt = require('bcrypt');
const Q = require('q');
const saltRounds = 10;

module.exports.create = function(data){
	return user.insert({
		email: data.email,
		userName: data.userName,
		fullName: data.fullName,
		r_token: mongoskin.ObjectID().toString(),
		createdOn: new Date()
	});
};

module.exports.findByEmail = function(email){
	return user.findOne({ email });
};

module.exports.findByRtoken = function(r_token){
	return user.findOne({ r_token });
};

module.exports.findByResettoken = function(reset_token){
	return user.findOne({ reset_token });
};

module.exports.setPassword = function(email, password){
	const defer = Q.defer();

	bcrypt.genSalt(saltRounds, function(err, salt) {
		if(!err){
		    bcrypt.hash(password, salt, function(err, hash) {
				user.updateOne(
					{ email	},
					{ $set: { hash }, $unset: { r_token: '' } }).then(function(){
					defer.resolve();
				}).catch(function(e){
					defer.reject(e);
				});
		    });
		}else {
			defer.reject(err);
		}
	});

	return defer.promise;
};

module.exports.setResetPassword = function(email, password){
	const defer = Q.defer();

	bcrypt.genSalt(saltRounds, function(err, salt) {
		if(!err){
		    bcrypt.hash(password, salt, function(err, hash) {
				user.updateOne(
					{ email	},
					{ $set: { hash }, $unset: { reset_token: '' } }).then(function(){
					defer.resolve();
				}).catch(function(e){
					defer.reject(e);
				});
		    });
		}else {
			defer.reject(err);
		}
	});

	return defer.promise;
};

module.exports.resetPassword = function(email){
	const defer = Q.defer();
	const reset_token = mongoskin.ObjectID().toString();

	user.updateOne(
		{ email },
		{ $set: { reset_token } }
	).then(function(){
		defer.resolve({ email, reset_token });
	}).catch(function(e){
		defer.reject(e);
	});
	
	return defer.promise;
};

module.exports.updateProfile = function(id, userName, fullName){
	const defer = Q.defer();

	user.updateOne(
		{ _id: id },
		{ $set: { userName, fullName } }
	).then(function(){
		defer.resolve();
	}).catch(function(e){
		defer.reject(e);
	});
	
	return defer.promise;
};

module.exports.getProfile = function(_id){
	return user.findOne({ _id }, {
		email: 1,
		fullName: 1,
		userName: 1
	});
};
