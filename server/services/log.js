const mongo = require('../mongo');
const MongoDataService = require('../infrastructure/mongoDataService');

const logs = new MongoDataService(mongo.Log);

module.exports = {
	insert(error, method, type) {
		return logs.insert({
			error,
			method,
			type,
			createdOn: new Date()
		});
	}
};
