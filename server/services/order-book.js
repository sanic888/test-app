const mongo = require('../mongo');
const MongoDataService = require('../infrastructure/mongoDataService');

const OrderBook = new MongoDataService(mongo.OrderBook);
const OrderBookBittrex = new MongoDataService(mongo.OrderBookBittrex);
const logService = require('./log');

const errorHandler = err => logService.insert(err.toString(), 'OrderBook');

module.exports = {
	// Exmo
	saveMany(orderBooks) {
		return orderBooks.reduce(
			(p, orderBook) => p.then(() => OrderBook.save(orderBook)),
			Promise.resolve()
		)
			.then(() => {})
			.catch(errorHandler);
		// return OrderBook.save(orderBook)
		// 	.then(() => {});
	},
	find(query = {}, options = {}) {
		return OrderBook.find(query, options)
			.then(orderBook => orderBook.toArray());
	},
	findOne(query = {}, options = {}) {
		return OrderBook.findOne(query, options);
	},

	// Bittrex
	saveManyBittrex(orderBook) {
		return OrderBookBittrex.save(orderBook)
			.then(() => {})
			.catch(errorHandler);
	},
	findBittrex(query = {}, options = {}) {
		return OrderBookBittrex.find(query, options)
			.then(orderBook => orderBook.toArray());
	},
};
