const mongo = require('../mongo');
const MongoDataService = require('../infrastructure/mongoDataService');

const AgendaJobs = new MongoDataService(mongo.AgendaJob);
const logService = require('./log');

const errorHandler = err => logService.insert(err.toString(), 'OPEN_ORDERS');

module.exports = {
	deleteMany() {
		return AgendaJobs.deleteMany({})
			.catch(errorHandler);
	}
};
