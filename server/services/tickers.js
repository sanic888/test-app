const mongo = require('../mongo');
const MongoDataService = require('../infrastructure/mongoDataService');

const Ticker = new MongoDataService(mongo.Ticker);
const TickerBittrex = new MongoDataService(mongo.TickerBittrex);
const logService = require('./log');

const errorHandler = err => {
    logService.insert(err.toString(), 'TICKER');
};

module.exports = {
    // Exmo
	saveMany(tickers) {
		const updates = [];
		tickers.forEach((ticker) => {
            updates.push({
                updateOne : { 
                    filter: {
                        pair: ticker.pair
                    }, 
                    update: ticker, 
                    upsert: true
                }
            });
		});

		return Ticker.bulkWrite(updates).catch(errorHandler);
	},
	find(query = {}, options = {}) {
		return Ticker.find(query, options)
			.then(tickers => tickers.toArray());
    },
    
    // Bittrex    
	saveBittrex(ticker) {
        return TickerBittrex.bulkWrite([
            {
                updateOne : { 
                    filter: {
                        pair: ticker.pair
                    }, 
                    update: ticker, 
                    upsert: true
                }
            }
        ]).then(() => {})
        .catch(errorHandler);
    },    
    findBittrex(query = {}, options = {}) {
		return TickerBittrex.find(query, options)
            .then(tickers => tickers.toArray());
    }
};
