const mongo = require('../mongo');
const MongoDataService = require('../infrastructure/mongoDataService');

const Trades = new MongoDataService(mongo.Trade);
const TradeBittrex = new MongoDataService(mongo.TradeBittrex);
const logService = require('./log');

const errorHandler = err => logService.insert(err.toString(), 'OPEN_ORDERS');

module.exports = {
	saveMany(trades) {
		const tradeIds = [];
		trades.forEach((userTrade) => {
			tradeIds.push(userTrade.trade_id);
		});

		return Trades.find({ trade_id: { $in: tradeIds } })
			.then(items => items.toArray())
			.then((items) => {
				items = items.map(o => o.trade_id);
				trades = trades.filter(userTrade => !items.includes(userTrade.trade_id));

				if (trades.length) {
					return Trades.insertMany(trades);
				}
			})
			.catch(errorHandler);
	},
	find(query = {}, options = {}) {
		return Trades.find(query, options)
			.then(trades => trades.toArray());
	},
	findOne(query = {}, options = {}) {
		return Trades.findOne(query, options);
	},
	
	// Bittrex
	saveManyBittrex(trades) {
		const tradeIds = [];
		trades.forEach((userTrade) => {
			tradeIds.push(userTrade.trade_id);
		});

		return TradeBittrex.find({ trade_id: { $in: tradeIds } })
			.then(items => items.toArray())
			.then((items) => {
				items = items.map(o => o.trade_id);
				trades = trades.filter(userTrade => !items.includes(userTrade.trade_id));

				if (trades.length) {
					return TradeBittrex.insertMany(trades);
				}
			})
			.catch(errorHandler);
	},
	findBittrex(query = {}, options = {}) {
		return TradeBittrex.find(query, options)
			.then(trades => trades.toArray());
	},
};
