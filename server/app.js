const express = require('express');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const agenda = require('./agenda');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');
const passport = require('passport');
const fs = require('fs');

require('./mongo');
require('./socket/socket');
require('./passport')(passport);

const store = new MongoDBStore({
	uri: 'mongodb://localhost:27017/connect_mongodb_session_test',
	collection: 'mySessions'
});

store.on('error', (error) => {
	console.log(error);
});

app.use(session({
	secret: '@mRU\'IE_[S3|EabYGg\'{IP-+JkPr(ck:cTeLkBQh2w9PyB@tV`\\;ZToDgbU\\E|s',
	resave: false,
	saveUninitialized: false,
	expires: new Date(Date.now() + (7 * 24* 60 * 60 * 1000)),
	cookie: { secure: false, maxAge: 7 * 24* 60 * 60 * 1000 },
	store
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.normalize(`${__dirname}./../dist`)));

require('./routes/route')(app);

const passJson = (req, html = 'main.html file not found') => {
	const json = {
		isAuthenticated: false,
	};

	if(req.isAuthenticated()) {
		json.isAuthenticated = true;
		json.email = req.user.email;
		json.userName = req.user.userName;
		json.fullName = req.user.fullName;
	}

	return html.replace('{{json}}', `<script>window.initJSON = ${JSON.stringify(json)}</script>`);	
};

app.get('/*', (req, res) => {
	fs.readFile(path.normalize(`${__dirname}./../dist/main.html`), 'utf8', (err, data) => {
		res.set('content-type', 'text/html');
		res.end(passJson(req, data));
	});
});

// Error handler
app.use(function(err, req, res, next) {
	if(err && err.status && err.stack) {
		res.status(err.status);
		res.end(JSON.stringify(err.stack));
	} else {
		next(err);
	}
});

agenda.init();

app.listen(3003, () => console.log('Server app listening on port 3003'));
