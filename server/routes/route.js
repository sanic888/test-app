const initRouter = require('./init/router');

module.exports = (app) => {
	app.use('/api/v1/init', initRouter);
	app.use('', require('./user/router'));
};
