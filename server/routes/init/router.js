const express = require('express');
const router = express.Router();
const controller = require('./controller');

router.get('/currencies', controller.getCurrencies);
router.get('/order_book', controller.getOrderBook);
router.get('/trades', controller.getTrades);
router.get('/tickers', controller.getTickers);
router.get('/locale', controller.getLocalization);

module.exports = router;
