const path = require('path');
const tradesService = require('../../services/trades');
const tickersService = require('../../services/tickers');
const orderBookService = require('../../services/order-book');
const logService = require('../../services/log');
const {
	curencies: { 'list': currencies }
} = require('../../exchanges/currencies');
const langsMap = {
	en: 'en.json',
	ru: 'ru.json'
};

const publicService = {
	getCurrencies(req, res) {
		res.json(currencies);
	},
	getOrderBook(req, res) {
		Promise.all([
			orderBookService.find({}, { limit: 1000, sort: { date: -1 } }),
			orderBookService.findBittrex({}, { limit: 1000, sort: { date: -1 } })
		])	
			.then(([orderBooksExmo, orderBooksBittrex]) => {
				res.json({
					exmo: orderBooksExmo,
					bittrex: orderBooksBittrex
				});
			})
			.catch((err) => {
				logService.insert(err.toString(), 'publicService:getOrderBook');
				res.status(500).json(err);
			});
	},
	getTrades(req, res) {
		Promise.all([
			tradesService.find({}, { limit: 1000, sort: { date: -1 } }),
			tradesService.findBittrex({}, { limit: 1000, sort: { date: -1 } })
		])		
			.then(([ tradesExmo, tradesBittrex ]) => {
				res.json({
					exmo: tradesExmo,
					bittrex: tradesBittrex
				});
			})
			.catch((err) => {
				logService.insert(err.toString(), 'publicService:getTrades');
				res.status(500).json(err);
			});
	},
	getTickers(req, res) {
		Promise.all([
			tickersService.find({}, { limit: 1000, sort: { date: -1 } }),
			tickersService.findBittrex({}, { limit: 1000, sort: { date: -1 } })
		])		
			.then(([ tickersExmo, tickersBittrex ]) => {
				res.json({
					exmo: tickersExmo,
					bittrex: tickersBittrex
				});
			})
			.catch((err) => {
				logService.insert(err.toString(), 'publicService:getTickers');
				res.status(500).json(err);
			});
	},
	getLocalization(req, res) {
		const config = require(path.normalize(`${__dirname}../../../../public/locales/${langsMap[req.query.lang]}`));
		res.json(config);
	}
};

module.exports = publicService;
