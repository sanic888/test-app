const Q = require('q');
const userService = require('../../services/user');
const contracts = require('../../infrastructure/contracts');
const messages = require('../../infrastructure/messages');

class UserContract {
	constructor() {
		this.userService = userService;
		this.contracts = contracts;
		this.messages = messages;
	}

	register(req){
		const defer = Q.defer();
		const email = req.body.email;
		const userName = req.body.userName;
		const fullName = req.body.fullName;

		if(!this.contracts.validateEmail(email)){
			defer.reject({message: this.messages.incorrectEmail});
		}

		this.userService.findByEmail(email).then(user => {
			if(user){
				defer.reject({message: this.messages.emailAlreadyExists});
			}else {
				defer.resolve({
					email,
					userName,
					fullName
				});			
			}
		});

		return defer.promise;
	}

	confirmRegister(req) {
		const defer = Q.defer();

		this.userService.findByRtoken(req.params.r_token).then(user => {
			if(!user){
				defer.reject({message: this.messages.confirmRegisterKeyNotFound});	
			}else {
				defer.resolve({});			
			}
		});

		return defer.promise;
	}

	confirmResetPassword(req) {
		const defer = Q.defer();

		this.userService.findByResettoken(req.params.reset_token).then(user => {
			if(!user){
				defer.reject({message: this.messages.ResetKeyNotFound});	
			}else {
				defer.resolve({});			
			}
		});

		return defer.promise;
	}

	resetPassword(req){
		const defer = Q.defer();
		const email = req.body.email;

		if(!this.contracts.validateEmail(email)){
			defer.reject({message: this.messages.incorrectEmail});
		}

		this.userService.findByEmail(email).then(user => {
			if(!user){
				defer.reject({message: this.messages.emailNotExists});
			}else {
				defer.resolve({
					email: email
				});			
			}
		});

		return defer.promise;
	}
}

module.exports = new UserContract();
