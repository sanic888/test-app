const userService = require('../../services/user');
const userContract = require('./contract');
const nodemailer = require('nodemailer');
const config = require('../../config');
const errorLogger = require('../../infrastructure/errorLogger');
const transporter = nodemailer.createTransport({
	host: 'smtp.gmail.com',
	port: 465,
	secure: true, // use SSL 
	auth: {
		user: config.mailer.fantasybettingvs.user,
		pass: config.mailer.fantasybettingvs.password
	},
	tls: {
		rejectUnauthorized: false
	}
});
const messages = require('../../infrastructure/messages');

class UserController {
	constructor() {
		this.messages = messages;
		this.transporter = transporter;
		this.errorLogger = errorLogger;
		this.config = config;
		this.userContract = userContract;
		this.userService = userService;
	}
	register(req, res) {
		return this.userContract.register(req).then(user => {
			return this.userService.create(user).then(data => {
				const mailOptions = {
					from: this.config.mailer.fantasybettingvs.name + ' <' + this.config.mailer.fantasybettingvs.user + '>', // sender address 
					to: user.email, // list of receivers 
					subject: 'Confirm Registration', // Subject line 
					html: '<a href="' + this.config.host + '/confirm-register/' + data.ops[0].r_token + '">Go to for registration</a>' // html body 
				};

				this.transporter.sendMail(mailOptions, e => {
					if(e){
						this.errorLogger.handle({message: e});
					}
				});
	
				res.status(200).json({ message: 'Please check your email inbox and confirm your address' });
			});
		}).catch(e => {
			this.errorLogger.handle(res, e);
			res.status(500).json({ message: e.message || this.messages.serverError });
		});
	}
	confirmRegister(req, res, next) {
		return this.userContract.confirmRegister(req).then(() => {
			next();
		}).catch(e => {
			this.errorLogger.handle(e);
			res.redirect('/');
		});
	}
	confirmResetPassword(req, res, next) {
		return this.userContract.confirmResetPassword(req).then(() => {
			next();
		}).catch(e => {
			this.errorLogger.handle(e);
			res.redirect('/');
		});
	}
	resetPassword(req, res) {
		return this.userContract.resetPassword(req).then(data => {
			return this.userService.resetPassword(data.email).then(user => {
				const mailOptions = {
					from: this.config.mailer.fantasybettingvs.name + ' <' + this.config.mailer.fantasybettingvs.user + '>', // sender address 
					to: user.email, // list of receivers 
					subject: 'Reset password', // Subject line 
					html: '<a href="' + this.config.host + '/reset-password/' + user.reset_token + '">Go to for reset password</a>' // html body 
				};

				this.transporter.sendMail(mailOptions, e => {
					if(e){
						this.errorLogger.handle({message: e});
					}
				});
	
				res.status(200).json({message: 'Please check your email, link for reset password is sent'});
			});
		}).catch(e => {
			this.errorLogger.handle(res, e);
			res.status(500).json({ message: e.message || this.messages.serverError });
		});
	}
	login(req, res) {
		res.json({
			email: req.user.email,
			userName: req.user.userName,
			fullName: req.user.fullName
		});
	}
	logout(req, res) {
		req.logOut();
		res.end();
	}
	updateProfile(req, res) {
		const id = req.user._id;
		const {
			userName,
			fullName
		} = req.body;

		return this.userService.updateProfile(id, userName, fullName).then(() => {
			res.status(200).end();
		}).catch(e => {
			this.errorLogger.handle(res, e);
			res.status(500).json({ message: e.message || this.messages.serverError });
		});
	}
	getProfile(req, res) {
		const id = req.user._id;

		return this.userService.getProfile(id).then((user) => {
			res.status(200).json(user);
		}).catch(e => {
			this.errorLogger.handle(res, e);
			res.status(500).json({ message: e.message || this.messages.serverError });
		});
	}
}

module.exports = new UserController();
