const express = require('express');
const router = express.Router();
const controller = require('./controller');
const passport = require('passport');

router.post('/api/v1/user/login', passport.authenticate('local-login', { session: true }), controller.login.bind(controller));
router.post('/api/v1/user/register', controller.register.bind(controller));
router.post('/api/v1/user/logout', controller.logout.bind(controller));
router.post('/api/v1/user/set-password', passport.authenticate('local-signup', { session: true }), controller.login.bind(controller));
router.post('/api/v1/user/reset-password', controller.resetPassword.bind(controller));
router.post('/api/v1/user/set-reset-password', passport.authenticate('local-reset-password', { session: true }), controller.login.bind(controller));
router.get('/reset-password/:reset_token', controller.confirmResetPassword.bind(controller));
router.get('/confirm-register/:r_token', controller.confirmRegister.bind(controller));
router.put('/api/v1/profile', controller.updateProfile.bind(controller));
router.get('/api/v1/profile', controller.getProfile.bind(controller));

module.exports = router;

/* function ensureAuthorized(req, res, next){
	let bearerToken;
	const bearerHeader = req.headers["authorization"];
	if (typeof bearerHeader !== 'undefined') {
		const bearer = bearerHeader.split(" ");
		bearerToken = bearer[1];
		req.token = bearerToken;
		next();
	} else {
		res.send(403);
	}
} */
