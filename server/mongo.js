const config = require('./config');
const mongo = require('mongoskin');
const { COLLECTIONS } = require('./infrastructure/constants');
const db = mongo.db(config.mongodb.uri, { native_parser: true });

module.exports = {
	User: db.collection(COLLECTIONS.USER),
	AgendaJob: db.collection(COLLECTIONS.AGENDA_JOB),
	Log: db.collection(COLLECTIONS.LOG),
	Trade: db.collection(COLLECTIONS.TRADE),
	TradeBittrex: db.collection(COLLECTIONS.TRADE_BITTREX),
	OrderBook: db.collection(COLLECTIONS.ORDER_BOOK),
	OrderBookBittrex: db.collection(COLLECTIONS.ORDER_BOOK_BITTREX),
	Ticker: db.collection(COLLECTIONS.TICKER),
	TickerBittrex: db.collection(COLLECTIONS.TICKER_BITTREX)
};
