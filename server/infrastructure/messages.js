module.exports = {
	incorrectEmail: 'Email is incorrect.',
	emailAlreadyExists: 'Email already exists.',
	emailNotExists: 'Email does\'t exists.',
	confirmRegisterKeyNotFound: 'Rtoken doesn\'t exist.',
	ResetKeyNotFound: 'Reset token doesn\'t exist.',
	passwordIsRequred: 'Password is required.',
	renewTokenError: 'Failed renew token.',
	passwordSuccessfullySet: 'Password was successfully set.',
	serverError: 'Server error.'
};
