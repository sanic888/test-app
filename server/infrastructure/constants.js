module.exports = {
	AGENDA_NAMES: {
		SYNC_EXMO_DATA: 'sync exmo data',
		SYNC_BITTREX_DATA: 'sync bittrex data'
	},
	COLLECTIONS: {
		USER: 'user',
		AGENDA_JOB: 'agenda-job',
		LOG: 'log',
		TRADE: 'trade',
		TRADE_BITTREX: 'trade-bittrex',
		ORDER_BOOK: 'order-book',
		ORDER_BOOK_BITTREX: 'order-book-bittrex',
		TICKER: 'ticker',
		TICKER_BITTREX: 'ticker-bittrex',
	},
	LOG_TYPES: {
		ERROR: 'error',
		SUCCESS: 'success'
	},
	LOG_MESSAGES: {
		// EXMO
		SYNC_EXMO_DATA: 'sync-exmo-data',
		SYNC_EXMO_DATA_GET_TRADES: 'sync-exmo-data:getTrades',
		SYNC_EXMO_DATA_CANT_PARSE: 'sync-exmo-data:cant-parse',
		SYNC_EXMO_DATA_GET_ORDER_BOOK: 'sync-exmo-data:getOrderBook',
		SYNC_EXMO_DATA_GET_TICKERS: 'sync-exmo-data:getTickers',
		
		// BITTREX
		SYNC_BITTREX_DATA: 'sync-bittrex-data',
		SYNC_BITTREX_DATA_GET_MARKET_HISTORY: 'sync-bittrex-data:getMarketHistory',
		SYNC_BITTREX_DATA_GET_ORDER_BOOK: 'sync-bittrex-data:getOrderBook',
		SYNC_BITTREX_DATA_GET_TICKERS: 'sync-bittrex-data:getTickers'
	}
};
