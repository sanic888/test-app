const Q = require('q');

const BaseDataService = function(col) {
	this._col = col;
};

BaseDataService.prototype = {
	findOne(query, options) {
		return this._perform('findOne', query, options);
	},
	find(query, options) {
		return this._perform('find', query, options);
	},
	insert(doc, options) {
		return this._perform('insert', doc, options);
	},
	insertMany(doc, options) {
		return this._perform('insertMany', doc, options);
	},
	updateOne(query, doc) {
		return this._perform('updateOne', query, doc);
	},
	updateMany(query, doc) {
		return this._perform('updateMany', query, doc);
	},
	update(query, doc) {
		return this._perform('update', query, doc);
	},
	remove(query, options) {
		return this._perform('remove', query, options);
	},
	deleteMany(query, options) {
		return this._perform('deleteMany', query, options);
	},
	save(query, options) {
		return this._perform('save', query, options);
	},
	bulkWrite(oprs, options) {
		return this._perform('bulkWrite', oprs, options);
	},
	_perform(method, query = {}, options = {}) {
		const deferred = Q.defer();

		this._col[method](query, options, (err, result) => {
			if (err) {
				deferred.reject(err);
			} else {
				deferred.resolve(result);
			}
		});

		return deferred.promise;
	}
};

module.exports = BaseDataService;
