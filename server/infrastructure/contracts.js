module.exports.validateEmail = function(email){
	const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // eslint-disable-line no-useless-escape
	return re.test(email);
};
module.exports.isString = function(val) {
	return Object.prototype.toString.call(val) === '[object String]';
};
module.exports.isEmpty = function(val) {
	return val === '' || val === undefined || val === null;
};
