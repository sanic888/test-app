const eventEmitter = require('../infrastructure/eventEmitter');
const orderBookService = require('../services/order-book');
const tradeService = require('../services/trades');
const tickerService = require('../services/tickers');
const logService = require('../services/log');
const io = require('socket.io')();
const { LOG_TYPES } = require('../infrastructure/constants');

io.on('connection', (client) => {
	// EXMO events
	eventEmitter.on('exmo:order-book:update', () => {
		orderBookService.find({})
			.then((orderBooks) => {
				client.emit('socket:exmo:order-book:update', orderBooks);
			})
			.catch((err) => {
				logService.insert(err.toString(), 'socket:exmo:order-book:update', LOG_TYPES.ERROR);
			});
	});
	eventEmitter.on('exmo:trades:update', () => {
		tradeService.find({}, { limit: 1000, sort: { date: -1 } })
			.then((trades) => {
				client.emit('socket:exmo:trades:update', trades);
			})
			.catch((err) => {
				logService.insert(err.toString(), 'socket:exmo:trades:update', LOG_TYPES.ERROR);
			});
	});
	eventEmitter.on('exmo:ticker:update', () => {
		tickerService.find({}, { limit: 1000, sort: { date: -1 } })
			.then((tickers) => {
				client.emit('socket:exmo:ticker:update', tickers);
			})
			.catch((err) => {
				logService.insert(err.toString(), 'socket:exmo:ticker:update', LOG_TYPES.ERROR);
			});
	});

	// BITTREX events
	eventEmitter.on('bittrex:order-book:update', () => {
		orderBookService.findBittrex({})
			.then((orderBooks) => {
				client.emit('socket:bittrex:order-book:update', orderBooks);
			})
			.catch((err) => {
				logService.insert(err.toString(), 'socket:bittrex:order-book:update', LOG_TYPES.ERROR);
			});
	});
	eventEmitter.on('bittrex:trades:update', () => {
		tradeService.findBittrex({}, { limit: 1000, sort: { date: -1 } })
			.then((trades) => {
				client.emit('socket:bittrex:trades:update', trades);
			})
			.catch((err) => {
				logService.insert(err.toString(), 'socket:bittrex:trades:update', LOG_TYPES.ERROR);
			});
	});
	eventEmitter.on('bittrex:ticker:update', () => {
		tickerService.findBittrex({}, { limit: 1000, sort: { date: -1 } })
			.then((trades) => {
				client.emit('socket:bittrex:ticker:update', trades);
			})
			.catch((err) => {
				logService.insert(err.toString(), 'socket:bittrex:ticker:update', LOG_TYPES.ERROR);
			});
	});
});

const port = 3333;
io.listen(port);
console.log('Socket listening on port ', port);
