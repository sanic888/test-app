module.exports = {
	curencies: {
		list: [
			// { id: 'ETH_LTC', name: 'ETH_LTC', direction: 2 },
			// { id: 'BCH_ETH', name: 'BCH_ETH', direction: 1 },
			// { id: 'XRP_BTC', name: 'XRP_BTC', direction: 1 },
			{ id: 'BTC_USD', name: 'BTC_USD', direction: 1 },
			{ id: 'XRP_USD', name: 'XRP_USD', direction: 1 },
			{ id: 'ETC_USD', name: 'ETC_USD', direction: 1 },
			{ id: 'ETH_USD', name: 'ETH_USD', direction: 1 },
			{ id: 'USDT_USD', name: 'USDT_USD', direction: 1 },
			{ id: 'LTC_USD', name: 'LTC_USD', direction: 1 },
			{ id: 'BCH_USD', name: 'BCH_USD', direction: 1 },
			{ id: 'ADA_USD', name: 'ADA_USD', direction: 1 },
			{ id: 'ZEC_USD', name: 'ZEC_USD', direction: 1 },
			{ id: 'TRX_USD', name: 'TRX_USD', direction: 1 },
			{ id: 'ZRX_USD', name: 'ZRX_USD', direction: 1 }
		],
		exmo: [
			// { id: 'ETH_LTC', name: 'ETH_LTC', direction: 1 },
			// { id: 'BCH_ETH', name: 'BCH_ETH', direction: 1 },
			// { id: 'XRP_BTC', name: 'XRP_BTC', direction: 1 },
			{ id: 'BTC_USD', name: 'BTC_USD', direction: 1 },
			{ id: 'XRP_USD', name: 'XRP_USD', direction: 1 },
			{ id: 'ETC_USD', name: 'ETC_USD', direction: 1 },
			{ id: 'ETH_USD', name: 'ETH_USD', direction: 1 },
			{ id: 'USDT_USD', name: 'USDT_USD', direction: 1 },
			{ id: 'LTC_USD', name: 'LTC_USD', direction: 1 },
			{ id: 'BCH_USD', name: 'BCH_USD', direction: 1 },
			{ id: 'ADA_USD', name: 'ADA_USD', direction: 1 },
			{ id: 'ZEC_USD', name: 'ZEC_USD', direction: 1 },
			{ id: 'TRX_USD', name: 'TRX_USD', direction: 1 },
			{ id: 'ZRX_USD', name: 'ZRX_USD', direction: 1 }
			
		],
		bittrex: [
			// { id: 'ETH_LTC', name: 'ETH-LTC', direction: 2 },
			// { id: 'BCH_ETH', name: 'ETH-BCH', direction: 1 },
			// { id: 'XRP_BTC', name: 'BTC-XRP', direction: 1 },
			{ id: 'BTC_USD', name: 'USD-BTC', direction: 1 },
			{ id: 'XRP_USD', name: 'USD-XRP', direction: 1 },
			{ id: 'ETC_USD', name: 'USD-ETC', direction: 1 },
			{ id: 'ETH_USD', name: 'USD-ETH', direction: 1 },
			{ id: 'USDT_USD', name: 'USD-USDT', direction: 1 },
			{ id: 'LTC_USD', name: 'USD-LTC', direction: 1 },
			{ id: 'BCH_USD', name: 'USD-BCH', direction: 1 },
			{ id: 'ADA_USD', name: 'USD-ADA', direction: 1 },
			{ id: 'ZEC_USD', name: 'USD-ZEC', direction: 1 },
			{ id: 'TRX_USD', name: 'USD-TRX', direction: 1 },
			{ id: 'ZRX_USD', name: 'USD-ZRX', direction: 1 }
		]
	}
};
