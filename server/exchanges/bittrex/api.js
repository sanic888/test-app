const request = require('request');
const Q = require('q');

const config = {
	url: 'https://bittrex.com/api/v1.1/public/'
};

const _getData = url => {
	const deferred = Q.defer();
	const options = {
		url: config.url + url,
		method: 'GET'
	};

	request(options, (error, response, body) => {
		try {
			const data = JSON.parse(body);
			if (!error && response.statusCode === 200 && data.success) {
				deferred.resolve(data.result);
			} else {
				deferred.reject(error);
			}
		} catch(e) {
			deferred.reject(error);
		}
	});

	return deferred.promise;
};

module.exports = {
	init(cfg) {
		// config.key = cfg.key;
		// config.secret = cfg.secret;
		config.nonce = Math.floor(new Date().getTime());
	},
	getMarketHistory(market) {
		return _getData('getmarkethistory?market=' + market);
	},
	getOrderBook(market) {
		return _getData('getorderbook?type=both&market=' + market);
	},
	getMarketSummary(market) {
		return _getData('getmarketsummary?market=' + market);
	}
};
