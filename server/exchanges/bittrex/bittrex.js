const api = require('./api');
const logService = require('../../services/log');
const eventEmitter = require('../../infrastructure/eventEmitter');

const tradesService = require('../../services/trades');
const orderBookService = require('../../services/order-book');
const tickerService = require('../../services/tickers');

const {
	LOG_TYPES,
	LOG_MESSAGES
} = require('../../infrastructure/constants');
const {
	curencies: { 'bittrex': currencies }
} = require('../currencies');

const syncService = {
	async synchronize() {
		currencies.forEach(async(currency) => {

			// get traders history
			await api.getMarketHistory(currency.name).then((data) => {
				const trades = data.map(item => {
					const date = new Date(item.TimeStamp);

					return {
						_id: item.Id,
						trade_id: item.Id,
						type: item.OrderType.toLowerCase(),
						quantity: item.Quantity.toString(),
						price: item.Price.toString(),
						amount: item.Total.toString(),
						date: parseInt(date.getTime() / 1000),
						pair: currency.id,
						saveDate: new Date(date)
					};
				});

				return tradesService.saveManyBittrex(trades);
			}).catch((err) => {
				!err && (err = 'ERROR IS EMRTY!!!!!');

				if (err.toString) {
					logService.insert(err.toString(), LOG_MESSAGES.SYNC_BITTREX_DATA_GET_MARKET_HISTORY, LOG_TYPES.ERROR);
				} else {
					logService.insert(err, LOG_MESSAGES.SYNC_BITTREX_DATA_GET_MARKET_HISTORY, LOG_TYPES.ERROR);
				}
			});

			// get order book
			await api.getOrderBook(currency.name).then((orderBook) => {
				orderBook.buy_top = orderBook.buy[0].Rate;
				orderBook.sell_top = orderBook.sell[0].Rate;
				orderBook.pair = currency.id;
				orderBook._id = currency.id;
				orderBook.buy = orderBook.buy.map(item => {
					return {
						price: item.Rate,
						quantity: item.Quantity,
						amount: item.Rate * item.Quantity
					};
				});
				orderBook.sell = orderBook.sell.map(item => {
					return {
						price: item.Rate,
						quantity: item.Quantity,
						amount: item.Rate * item.Quantity
					};
				});
				orderBook.saveDate = new Date();

				return orderBookService.saveManyBittrex(orderBook);
			}).catch((err) => {
				!err && (err = 'ERROR IS EMRTY!!!!!');

				if (err.toString) {
					logService.insert(err.toString(), LOG_MESSAGES.SYNC_BITTREX_DATA_GET_ORDER_BOOK, LOG_TYPES.ERROR);
				} else {
					logService.insert(err, LOG_MESSAGES.SYNC_BITTREX_DATA_GET_ORDER_BOOK, LOG_TYPES.ERROR);
				}
			});

			// get ticker
			await api.getMarketSummary(currency.name).then((data) => {
				const ticker = {};
				if (data[0]) {
					ticker.buy_price = 1 / data[0].Ask;
					ticker.sell_price = 1 / data[0].Bid;
					ticker.last_trade = 1 / data[0].Last;
					ticker.high = 1 / data[0].Low;
					ticker.low = 1 / data[0].High;
					ticker.avg = 1 / data[0].PrevDay;
					ticker.vol = data[0].Volume;
					ticker.vol_curr = data[0].BaseVolume;
					ticker.updated = parseInt(new Date(data[0].TimeStamp).getTime() / 1000);
					ticker.pair = currency.id;

					return tickerService.saveBittrex(ticker);
				}
			}).catch((err) => {
				!err && (err = 'ERROR IS EMRTY!!!!!');

				if (err.toString) {
					logService.insert(err.toString(), LOG_MESSAGES.SYNC_BITTREX_DATA_GET_TICKERS, LOG_TYPES.ERROR);
				} else {
					logService.insert(err, LOG_MESSAGES.SYNC_BITTREX_DATA_GET_TICKERS, LOG_TYPES.ERROR);
				}
			});
		});

		eventEmitter.emit('bittrex:order-book:update');
		eventEmitter.emit('bittrex:trades:update');
		eventEmitter.emit('bittrex:ticker:update');
	}
};

module.exports.syncData = () => syncService.synchronize()
	.catch((err) => {
		!err && (err = 'ERROR IS EMRTY!!!!!');

		if (err.toString) {
			logService.insert(err.toString(), LOG_MESSAGES.SYNC_BITTREX_DATA, LOG_TYPES.ERROR);
		} else {
			logService.insert(err, LOG_MESSAGES.SYNC_BITTREX_DATA, LOG_TYPES.ERROR);
		}
	});
