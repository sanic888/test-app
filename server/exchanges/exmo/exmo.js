const api = require('./api');
const tradesService = require('../../services/trades');
const orderBookService = require('../../services/order-book');
const tickersService = require('../../services/tickers');
const logService = require('../../services/log');
const eventEmitter = require('../../infrastructure/eventEmitter');
const {
	curencies: { 'exmo': currencies }
} = require('../currencies');
const config = require('../../config.json');
const {
	LOG_TYPES,
	LOG_MESSAGES
} = require('../../infrastructure/constants');
const CURRENCIES = currencies.map((currency) => currency.name);

api.init(); // { key: `K-${config.int.ekey}`, secret: `S-${config.int.esecret}` }

const syncService = {
	getTrades() {
		const pair = CURRENCIES.join(',');

		return api.api_query_public(`trades?pair=${pair}&limit=1000`)
			.then((trades) => {
				if (trades.error) {
					logService.insert(trades.error.toString(), LOG_MESSAGES.SYNC_EXMO_DATA_GET_TRADES, LOG_TYPES.ERROR);
					return;
				}

				let allUserTrades = [];
				CURRENCIES.forEach((curr) => {
					trades[curr].forEach((trade) => {
						const date = new Date(+trade.date * 1000);
						const saveDate = new Date(date.valueOf() + date.getTimezoneOffset() * 60000);
						trade._id = trade.trade_id;
						trade.pair = curr;
						trade.saveDate = saveDate;
						trade.date = saveDate / 1000;
					});

					allUserTrades = allUserTrades.concat(trades[curr]);
				});

				return tradesService.saveMany(allUserTrades);
			})
			.then(() => eventEmitter.emit('exmo:trades:update'))
			.catch((err) => {
				!err && (err = 'ERROR IS EMRTY!!!!!');
		
				if (err.toString) {
					logService.insert(err.toString(), LOG_MESSAGES.SYNC_EXMO_DATA_GET_TRADES, LOG_TYPES.ERROR);
				} else {
					logService.insert(err, LOG_MESSAGES.SYNC_EXMO_DATA_GET_TRADES, LOG_TYPES.ERROR);
				}
			});
	},
	getOrderBook() {
		const pair = CURRENCIES.join(',');

		return api.api_query_public(`order_book?pair=${pair}&limit=1000`)
			.then((orderBook) => {
				if (orderBook.error) {
					logService.insert(orderBook.error.toString(), LOG_MESSAGES.SYNC_EXMO_DATA_GET_ORDER_BOOK, LOG_TYPES.ERROR);
					return;
				}

				const allOrderBooks = [];
				CURRENCIES.forEach((curr) => {
					orderBook[curr].pair = curr;
					orderBook[curr]._id = curr;
					orderBook[curr].saveDate = new Date();
					orderBook[curr].buy = orderBook[curr].bid.map(item => {
						return {
							price: +item[0],
							quantity: +item[1],
							amount: +item[2]
						};
					});
					orderBook[curr].sell = orderBook[curr].ask.map(item => {
						return {
							price: +item[0],
							quantity: +item[1],
							amount: +item[2]
						};
					});
					orderBook[curr].buy_top = orderBook[curr].bid_top;
					orderBook[curr].sell_top = orderBook[curr].ask_top;
					delete orderBook[curr].bid;
					delete orderBook[curr].ask;
					delete orderBook[curr].bid_top;
					delete orderBook[curr].ask_top;

					allOrderBooks.push(orderBook[curr]);
				});

				return orderBookService.saveMany(allOrderBooks);
			})
			.then(() => eventEmitter.emit('exmo:order-book:update'))
			.catch((err) => {
				!err && (err = 'ERROR IS EMRTY!!!!!');
		
				if (err.toString) {
					logService.insert(err.toString(), LOG_MESSAGES.SYNC_EXMO_DATA_GET_ORDER_BOOK, LOG_TYPES.ERROR);
				} else {
					logService.insert(err, LOG_MESSAGES.SYNC_EXMO_DATA_GET_ORDER_BOOK, LOG_TYPES.ERROR);
				}
			});
	},
	getTickers() {
		return api.api_query_public(`ticker`)
			.then((data) => { // tickers
				const tickers = CURRENCIES.map((curr) => {
					data[curr].pair = curr;
					return data[curr];
				});
				
				return tickersService.saveMany(tickers);
			})
			.then(() => eventEmitter.emit('exmo:ticker:update'))
			.catch((err) => {
				!err && (err = 'ERROR IS EMRTY!!!!!');
		
				if (err.toString) {
					logService.insert(err.toString(), LOG_MESSAGES.SYNC_EXMO_DATA_GET_TICKERS, LOG_TYPES.ERROR);
				} else {
					logService.insert(err, LOG_MESSAGES.SYNC_EXMO_DATA_GET_TICKERS, LOG_TYPES.ERROR);
				}
			});
	}
};

module.exports.syncData = () => syncService.getTrades()
	.then(() => syncService.getOrderBook())
	.then(() => syncService.getTickers())
	.catch((err) => {
		!err && (err = 'ERROR IS EMRTY!!!!!');

		if (err.toString) {
			logService.insert(err.toString(), LOG_MESSAGES.SYNC_EXMO_DATA, LOG_TYPES.ERROR);
		} else {
			logService.insert(err, LOG_MESSAGES.SYNC_EXMO_DATA, LOG_TYPES.ERROR);
		}
	});
