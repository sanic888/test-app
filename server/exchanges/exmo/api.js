const CryptoJS = require('crypto-js');
const querystring = require('querystring');
const request = require('request');
const Q = require('q');
const logService = require('../../services/log');
const {
	LOG_TYPES,
	LOG_MESSAGES
} = require('../../infrastructure/constants');
const config = {
	url: 'https://api.exmo.com/v1/'
};

// const sign = message => CryptoJS.HmacSHA512(message, config.secret).toString(CryptoJS.enc.hex);

module.exports = {
	init(cfg) {
		// config.key = cfg.key;
		// config.secret = cfg.secret;
		config.nonce = Math.floor(new Date().getTime());
	},
	// api_query(methodName, data) {
	// 	const deferred = Q.defer();


	// 	data.nonce = config.nonce++;
	// 	const postData = querystring.stringify(data);

	// 	const options = {
	// 		url: config.url + methodName,
	// 		method: 'POST',
	// 		headers: {
	// 			Key: config.key,
	// 			Sign: sign(postData)
	// 		},
	// 		form: data
	// 	};

	// 	request(options, (error, response, body) => {
	// 		if (!error && response.statusCode === 200) {
	// 			try {
	// 				const jsonData = JSON.parse(body);
	// 				deferred.resolve(jsonData);
	// 			} catch(err) {
	// 				!body && (body = err);
	// 				logService.insert(body, LOG_MESSAGES.SYNC_EXMO_DATA_CANT_PARSE, LOG_TYPES.ERROR);
	// 				deferred.reject(err);
	// 			}
	// 			deferred.resolve(body);
	// 		} else {
	// 			deferred.reject(error);
	// 		}
	// 	});

	// 	return deferred.promise;
	// },
	api_query_public(methodName) {
		const deferred = Q.defer();

		const options = {
			url: config.url + methodName,
			method: 'GET'
		};

		request(options, (error, response, body) => {
			if (!error && response.statusCode === 200) {
				try {
					const jsonData = JSON.parse(body);
					deferred.resolve(jsonData);
				} catch(err) {
					!body && (body = err);
					logService.insert(body, LOG_MESSAGES.SYNC_EXMO_DATA_CANT_PARSE, LOG_TYPES.ERROR);
					deferred.reject(err);
				}
			} else {
				deferred.reject(error);
			}
		});

		return deferred.promise;
	},
	test() {
		return config.key;
	}
};
