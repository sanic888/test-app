const syncExmoService = require('./exchanges/exmo/exmo');
const syncBittrexService = require('./exchanges/bittrex/bittrex');
const Agenda = require('agenda');
const config = require('./config.json');
const {
	COLLECTIONS: {
		AGENDA_JOB
	},
	LOG_TYPES,
	LOG_MESSAGES,
	AGENDA_NAMES
} = require('./infrastructure/constants');
const agenda = new Agenda({ db: { address: config.mongodb.uri, collection: AGENDA_JOB } });
const logService = require('./services/log');
const agendaJobsService = require('./services/agenda-jobs');

let n = 0;

const consoleLog = (name) => {
	n++;
	const date = new Date();
	console.log(`${n} ${name} : ${date.getHours()}:${date.getMinutes() + 1}:${date.getSeconds()}`);
};

agenda.define(AGENDA_NAMES.SYNC_EXMO_DATA, (job, done) => {
	consoleLog(AGENDA_NAMES.SYNC_EXMO_DATA);

	syncExmoService.syncData()
		.then(() => {
			logService.insert({}, LOG_MESSAGES.SYNC_EXMO_DATA, LOG_TYPES.SUCCESS);
		})
		.then(done)
		.catch((err) => {
			!err && (err = 'ERROR IS EMRTY!!!!!');

			if (err.toString) {
				logService.insert(err.toString(), LOG_MESSAGES.SYNC_EXMO_DATA, LOG_TYPES.ERROR);
			} else {
				logService.insert(err, LOG_MESSAGES.SYNC_EXMO_DATA, LOG_TYPES.ERROR);
			}

			done();
		});
});

agenda.define(AGENDA_NAMES.SYNC_BITTREX_DATA, (job, done) => {
	consoleLog(AGENDA_NAMES.SYNC_BITTREX_DATA);

	syncBittrexService.syncData()
		.then(() => {
			logService.insert({}, LOG_MESSAGES.SYNC_BITTREX_DATA, LOG_TYPES.SUCCESS);
		})
		.then(done)
		.catch((err) => {
			!err && (err = 'ERROR IS EMRTY!!!!!');

			if (err.toString) {
				logService.insert(err.toString(), LOG_MESSAGES.SYNC_BITTREX_DATA, LOG_TYPES.ERROR);
			} else {
				logService.insert(err, LOG_MESSAGES.SYNC_BITTREX_DATA, LOG_TYPES.ERROR);
			}

			done();
		});
});

module.exports = {
	async init() {
		await agendaJobsService.deleteMany();
		await agenda.start();
		await agenda.every('20 seconds', AGENDA_NAMES.SYNC_EXMO_DATA);
		await agenda.every('20 seconds', AGENDA_NAMES.SYNC_BITTREX_DATA);
	}
};
