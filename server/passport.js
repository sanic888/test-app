const LocalStrategy = require('passport-local').Strategy;
const userService = require('./services/user');
const bcrypt = require('bcrypt');

module.exports = (passport) => {
	passport.serializeUser((user, done) => {
		done(null, user);
	});

	passport.deserializeUser((user, done) => {
		done(null, user);
	});

	passport.use('local-reset-password', new LocalStrategy({
		usernameField: 'token',
		passwordField: 'password',
		passReqToCallback: true
	}
	, (req, token, password, done) => {
		userService.findByResettoken(token).then((user) => {
			if(!user){
				return done({ status: 422,  stack: { message: 'This user doesn\'t exist.' } });
			}else {
				userService.setResetPassword(user.email, password).then(() => {
					return done(null, {
						_id: user._id,
						email: user.email,
						userName: user.userName,
						fullName: user.fullName
					});
				});		
			}
		});
	}));

	passport.use('local-signup', new LocalStrategy({
		usernameField: 'token',
		passwordField: 'password',
		passReqToCallback: true
	}
	, (req, token, password, done) => {
		userService.findByRtoken(token).then((user) => {
			if(!user){
				return done({ status: 422,  stack: { message: 'This user doesn\'t exist.' } });
			}else {
				userService.setPassword(user.email, password).then(() => {
					return done(null, {
						_id: user._id,
						email: user.email,
						userName: user.userName,
						fullName: user.fullName
					});
				});		
			}
		});
	}));

	passport.use('local-login', new LocalStrategy({
		usernameField: 'username',
		passwordField: 'password',
		passReqToCallback: true
	}, (req, username, password, done) => {
		userService.findByEmail(username).then((user) => {
			if (!user || user.r_token) {
				return done({ status: 422,  stack: { message: 'This user doesn\'t exist.' } });
			}
			
			bcrypt.compare(password, user.hash, (err, res) => {
				if (err) {
					return done(err);
				}

				if(res === true){
					return done(null, {
						_id: user._id,
						email: user.email,
						userName: user.userName,
						fullName: user.fullName
					});
				}else {
					return done({ status: 422,  stack: { message: 'Incorrect password.' } });
				}
			});
		}).catch((err) => err && done(err));
	}));
};
