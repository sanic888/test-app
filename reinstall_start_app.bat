set HOME=%USERPROFILE%
set PATH=%PATH%;c:\Program Files\Git\bin\
set BRANCH=master

git checkout %BRANCH%

rmdir node_modules /s /q
call npm install

start call mongod --dbpath ./db &
start call npm run server &
start call npm run dev
