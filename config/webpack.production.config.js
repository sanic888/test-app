const webpack = require('webpack');
const helpers = require('./helpers');
const glob = require('glob');
const Config = require('webpack-config').Config;
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const PurgecssPlugin = require('purgecss-webpack-plugin');
const ENV = process.env.NODE_ENV = process.env.ENV = 'production';

module.exports = new Config().extend(helpers.root('config', 'webpack.common.config.js')).merge({
	output: {
		path: helpers.root('dist'),
		publicPath: '/',
		filename: '[name].[hash].js'
	},

	module: {
		rules: [{
			test: /\.scss$/,
			use: [
				MiniCssExtractPlugin.loader,
				'css-loader',
				'postcss-loader',
				'sass-loader'
			]
		}]
	},

	optimization: {
		mangleWasmImports: true,
		minimizer: [
			new UglifyJsPlugin({
				cache: true,
				parallel: true,
				uglifyOptions: {
					compress: true,
					ecma: 6,
					mangle: true
				}
			})
		],
		splitChunks: {
			cacheGroups: {
				vendor: {
					test: /node_modules/,
					name: 'vendor',
					chunks: 'initial',
					enforce: true
				}
			}
		}
	},

	plugins: [
		new webpack.NoEmitOnErrorsPlugin(),

		new webpack.DefinePlugin({
			'process.env': {
				'ENV': JSON.stringify(ENV)
			}
		}),

		new MiniCssExtractPlugin({
			filename: '[name].[hash].css',
			chunkFilename: '[id].[hash].css'
		}),

		new PurgecssPlugin({
			fontFace: true,
			keyframes: true,
			paths: glob.sync(helpers.root('src', '**', '*'), {nodir: true})
		})
	]
});
