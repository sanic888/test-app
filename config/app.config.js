export default {
	"web": {
		"host": "localhost",
		"port": "3003",
		"basePath": "/api/v1/"
	},
	"socket": {
		"host": "http://localhost",
		"port": "3333"
	}
};
