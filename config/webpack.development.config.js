const webpack = require('webpack');
const helpers = require('./helpers');
const Config = require('webpack-config').Config;
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ENV = process.env.ENV = process.env.NODE_ENV = 'development';

module.exports = new Config().extend(helpers.root('config', 'webpack.common.config.js')).merge({
	// devtool: 'cheap-module-eval-source-map', TODO:// Need use this one when bug will be fixed https://github.com/webpack-contrib/mini-css-extract-plugin/issues/29
	devtool: 'cheap-module-source-map',

	watch: true,

	output: {
		path: helpers.root('dist'),
		publicPath: '/',
		filename: '[name].js',
		chunkFilename: '[id].chunk.js'
	},

	module: {
		rules: [{
			test: /\.scss$/,
			use: ExtractTextPlugin.extract({
				fallback: 'style-loader',
				use: [
					{
						loader: 'css-loader',
						options: {sourceMap: true}
					},
					{
						loader: 'postcss-loader',
						options: {sourceMap: true}
					},
					{
						loader: 'sass-loader',
						options: {
							includePaths: [helpers.root('src', 'styles')],
							sourceMap: true
						}
					}
				]
			})
		}]
	},

	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				'ENV': JSON.stringify(ENV)
			}
		}),

		new ExtractTextPlugin('[name].css')
	]
});
