const webpack = require('webpack');
const helpers = require('./helpers');
const Config = require('webpack-config').Config;
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = new Config().merge({
    entry: {
        app: helpers.root('src', 'index.js')
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                use: 'file-loader?name=assets/[name].[hash].[ext]'
            }
        ]
    },

    plugins: [
        new CleanWebpackPlugin(
            [helpers.root('dist')],
            { root: helpers.root() }
        ),

        new HtmlWebpackPlugin({
            template: helpers.root('main.html'),
            filename: 'main.html'
        }),

        new webpack.ProvidePlugin({
            jQuery: 'jquery'
        }),

        new CopyWebpackPlugin([
            { from: helpers.root('public', 'images'), to: 'images' },
            { from: helpers.root('public', 'fonts'), to: 'fonts' },
            { from: helpers.root('favicon.ico'), to: 'favicon.ico' }
        ])
    ]
});
